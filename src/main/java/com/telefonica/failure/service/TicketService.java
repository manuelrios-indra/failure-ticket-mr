package com.telefonica.failure.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.net.ssl.SSLEngineResult.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.failure.commons.StatusFactory;
import com.telefonica.failure.commons.Ticket;
import com.telefonica.failure.commons.Util;
import com.telefonica.failure.entity.ALDM;
import com.telefonica.failure.entity.CMS;
import com.telefonica.failure.entity.Gestel;
import com.telefonica.failure.repository.ALDMRepository;
import com.telefonica.failure.repository.CMSRepository;
import com.telefonica.failure.repository.GestelRepository;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.RelatedObjectType;
import com.telefonica.failure.types.RelatedPartyType;
import com.telefonica.failure.types.TicketDetailType;
import com.telefonica.failure.types.TicketNoteInfoType;
import com.telefonica.failure.types.RelatedObjectType.InvolvementEnum;
import com.telefonica.failure.types.TicketDetailType.StatusEnum;
import com.telefonica.failure.exception.BusinessException;

@Service
public class TicketService implements ITicketService {

    @Autowired
    ALDMRepository aLDMRepository;

    @Autowired
    CMSRepository cMSRepository;

    @Autowired
    GestelRepository gestelRepository;

    public List<TicketDetailType> retrieveTickets(String type, String relatedObjectInvolvement, String relatedObjectReference) {

	List<TicketDetailType> listTicket = new ArrayList<>();

	if (type.equals(Ticket.BREAKDOWN)) {

	    switch (relatedObjectInvolvement) {
	    // Cms
	    case Ticket.CABLETV:
	    case Ticket.BROADBAND:
		listTicket = createTicketDetailTypeListFromCMS(relatedObjectReference);
		break;
	    // Gestel
	    case Ticket.LANDLINE:
		listTicket = createTicketDetailTypeListFromGestel(relatedObjectReference);
		break;
	    // Amdocs
	    case Ticket.MOBILE:
		listTicket = createTicketDetailTypeListFromALDM(relatedObjectReference);
		break;
	    default:
		throw new BusinessException(Ticket.INCORRECTROI.concat(relatedObjectInvolvement));
	    }

	}

	else {

	    throw new BusinessException(Ticket.INCORRECTTYPE.concat(type));

	}

	if (listTicket == null || listTicket.isEmpty()) {
	    throw new BusinessException(Ticket.TICKETNOTFOUND.concat(relatedObjectReference));
	}
	return listTicket;

    }

    public List<TicketDetailType> createTicketDetailTypeListFromALDM(String relatedObjectReference) {

	return aLDMRepository.findByTelephoneNumberAndFailureDateAfter(relatedObjectReference, Util.threeMonthsAgo()).stream()
		.map(this::toTicketDetailTypeFromALDM).collect(Collectors.toList());

    }

    private TicketDetailType toTicketDetailTypeFromALDM(ALDM aldm) {
	TicketDetailType ticket = new TicketDetailType();

	ChannelRefType statusChangeChannel = new ChannelRefType();
	statusChangeChannel.setId(aldm.getFailureLastChangeUserChannel());
	statusChangeChannel.setName(aldm.getFailureLastChangeUserChannel());

	ChannelRefType resolutionChannel = new ChannelRefType();
	resolutionChannel.setId(aldm.getResolutionUserChannel());
	resolutionChannel.setName(aldm.getResolutionUserChannel());

	RelatedPartyType relatedParty1 = new RelatedPartyType();
	relatedParty1.setRole(Ticket.CREATIONUSER);
	relatedParty1.setId(aldm.getFailureRegisteredUser());
	RelatedPartyType relatedParty2 = new RelatedPartyType();
	relatedParty2.setRole(Ticket.SATUSCHANGEUSER);
	relatedParty2.setId(aldm.getFailureLastChangeUser());
	RelatedPartyType relatedParty3 = new RelatedPartyType();
	relatedParty3.setRole(Ticket.RESOLUTIONUSER);
	relatedParty3.setId(aldm.getResolutionUser());
	List<RelatedPartyType> relatedParty = Arrays.asList(relatedParty1, relatedParty2, relatedParty3);

	RelatedObjectType relatedObject1 = new RelatedObjectType();
	relatedObject1.setInvolvement(InvolvementEnum.MOBILE);
	relatedObject1.setReference(aldm.getTelephoneNumber());
	RelatedObjectType relatedObject2 = new RelatedObjectType();
	relatedObject2.setInvolvement(InvolvementEnum.LANDLINE);
	RelatedObjectType relatedObject3 = new RelatedObjectType();
	relatedObject3.setInvolvement(InvolvementEnum.CABLETV);
	RelatedObjectType relatedObject4 = new RelatedObjectType();
	relatedObject4.setInvolvement(InvolvementEnum.BROADBAND);
	RelatedObjectType relatedObject5 = new RelatedObjectType();
	relatedObject5.setInvolvement(InvolvementEnum.BILL);
	List<RelatedObjectType> relatedObject = Arrays.asList(relatedObject1, relatedObject2, relatedObject3, relatedObject4,
		relatedObject5);

	TicketNoteInfoType note1 = new TicketNoteInfoType();
	note1.setDate(Util.getOffsetDateTime(aldm.getFailureDate()));
	note1.setAuthor(aldm.getFailureRegisteredUser());
	note1.setText(aldm.getFailureRemark());
	TicketNoteInfoType note2 = new TicketNoteInfoType();
	note2.setDate(Util.getOffsetDateTime(aldm.getResolutionDate()));
	note2.setAuthor(aldm.getResolutionUser());
	note2.setText(aldm.getResolutionUserRemark());
	List<TicketNoteInfoType> note = Arrays.asList(note1, note2);

	ChannelRefType channel = new ChannelRefType();
	channel.setId(aldm.getFailureRegisteredChannel());
	channel.setName(aldm.getFailureRegisteredChannel());

	KeyValueType additionalData1 = new KeyValueType();
	additionalData1.setKey(Ticket.INSCRIPTION);
	KeyValueType additionalData2 = new KeyValueType();
	additionalData2.setKey(Ticket.CLAIMNUMBER);
	KeyValueType additionalData3 = new KeyValueType();
	additionalData3.setKey(Ticket.REPORTNUMBER);
	KeyValueType additionalData4 = new KeyValueType();
	additionalData4.setKey(Ticket.INDICATOR);
	KeyValueType additionalData5 = new KeyValueType();
	additionalData5.setKey(Ticket.DATEMASSBREAKDOWN);
	KeyValueType additionalData6 = new KeyValueType();
	additionalData6.setKey(Ticket.SCHEDULEDWORKCODE);
	KeyValueType additionalData7 = new KeyValueType();
	additionalData7.setKey(Ticket.SCHEDULEDWORKDATE);
	KeyValueType additionalData8 = new KeyValueType();
	additionalData8.setKey(Ticket.SCHEDULEDWORKENDTIME);
	List<KeyValueType> additionalData = Arrays.asList(additionalData1, additionalData2, additionalData3, additionalData4,
		additionalData5, additionalData6, additionalData7, additionalData8);

	String id = String.valueOf(aldm.getFailureALDMId());

	ticket.setId(id);
	ticket.setHref(Ticket.HREF.concat(id));
	ticket.setDescription(aldm.getFailureReasonDescription());
	ticket.setCustomerId(aldm.getClientCode());
	ticket.setAccountId(aldm.getAccountCode());
	ticket.setCreationDate(Util.getOffsetDateTime(aldm.getFailureDate()));
	ticket.setType(Ticket.BREAKDOWN);
	ticket.setSource(Ticket.ALDM);
	if(aldm.getFailureStatusDescription() != null) {
	ticket.setStatus(StatusEnum.fromValue(StatusFactory.fromALDM(aldm.getFailureStatusDescription())));}
	ticket.setSubStatus(aldm.getFailureStatusDescription());
	ticket.setStatusChangeDate(Util.getOffsetDateTime(aldm.getFailureLastChangeDate()));
	ticket.setStatusChangeChannel(statusChangeChannel);
	ticket.setResolutionDate(Util.getOffsetDateTime(aldm.getResolutionDate()));// --
	ticket.setResolutionChannel(resolutionChannel);
	ticket.setResolution(aldm.getResolutionDescription());// ----
	ticket.setRelatedParty(relatedParty);
	ticket.setRelatedObject(relatedObject);
	ticket.setAdditionalData(additionalData);
	ticket.setNote(note);
	ticket.setChannel(channel);

	return ticket;
    }

    public List<TicketDetailType> createTicketDetailTypeListFromCMS(String relatedObjectReference) {
	return cMSRepository.findByTvServiceCodeAndFailureDateAfter(relatedObjectReference, Util.threeMonthsAgo()).stream()
		.map(this::toTicketDetailTypeFromCMS).collect(Collectors.toList());
    }

    private TicketDetailType toTicketDetailTypeFromCMS(CMS cms) {
	TicketDetailType ticket = new TicketDetailType();

	ChannelRefType statusChangeChannel = new ChannelRefType();
	statusChangeChannel.setId(cms.getFailureLastChangeUserChannel());
	statusChangeChannel.setName(cms.getFailureLastChangeUserChannel());

	StringBuilder resolution = new StringBuilder();
	resolution.append(cms.getResolutionCode()).append(Ticket.HYPHEN).append(cms.getResolutionDescription());
	resolution.append(Ticket.VERTICALLINE);
	resolution.append(cms.getResolutionReasonCode()).append(Ticket.HYPHEN).append(cms.getResolutionReasonDescription());

	ChannelRefType resolutionChannel = new ChannelRefType();
	resolutionChannel.setId(cms.getResolutionUserChannel());
	resolutionChannel.setName(cms.getResolutionUserChannel());

	RelatedPartyType relatedParty1 = new RelatedPartyType();
	relatedParty1.setRole(Ticket.CREATIONUSER);
	relatedParty1.setId(cms.getFailureRegisteredUser());
	RelatedPartyType relatedParty2 = new RelatedPartyType();
	relatedParty2.setRole(Ticket.SATUSCHANGEUSER);
	relatedParty2.setId(cms.getFailureLastChangeUser());
	RelatedPartyType relatedParty3 = new RelatedPartyType();
	relatedParty3.setRole(Ticket.RESOLUTIONUSER);
	relatedParty3.setId(cms.getResolutionUser());
	List<RelatedPartyType> relatedParty = Arrays.asList(relatedParty1, relatedParty2, relatedParty3);

	RelatedObjectType relatedObject1 = new RelatedObjectType();
	relatedObject1.setReference(cms.getTelephoneNumber());
	RelatedObjectType relatedObject2 = new RelatedObjectType();
	relatedObject2.setInvolvement(InvolvementEnum.LANDLINE);
	RelatedObjectType relatedObject3 = new RelatedObjectType();
	relatedObject3.setInvolvement(InvolvementEnum.CABLETV);
	relatedObject3.setReference(cms.getTvServiceCode());
	RelatedObjectType relatedObject4 = new RelatedObjectType();
	relatedObject4.setInvolvement(InvolvementEnum.BROADBAND);
	relatedObject4.setReference(cms.getTvServiceCode());
	RelatedObjectType relatedObject5 = new RelatedObjectType();
	relatedObject5.setInvolvement(InvolvementEnum.BILL);
	List<RelatedObjectType> relatedObject = Arrays.asList(relatedObject1, relatedObject2, relatedObject3, relatedObject4,
		relatedObject5);

	TicketNoteInfoType note1 = new TicketNoteInfoType();
	note1.setDate(Util.getOffsetDateTime(cms.getFailureDate()));
	note1.setAuthor(cms.getFailureRegisteredUser());
	note1.setText(cms.getFailureRemark());
	TicketNoteInfoType note2 = new TicketNoteInfoType();
	note2.setDate(Util.getOffsetDateTime(cms.getResolutionDate()));
	note2.setAuthor(cms.getResolutionUser());
	note2.setText(cms.getResolutionUserRemark());
	List<TicketNoteInfoType> note = Arrays.asList(note1, note2);

	ChannelRefType channel = new ChannelRefType();
	channel.setId(cms.getFailureRegisteredChannel());
	channel.setName(cms.getFailureRegisteredChannel());

	KeyValueType additionalData1 = new KeyValueType();
	additionalData1.setKey(Ticket.INSCRIPTION);
	KeyValueType additionalData2 = new KeyValueType();
	additionalData2.setKey(Ticket.CLAIMNUMBER);
	KeyValueType additionalData3 = new KeyValueType();
	additionalData3.setKey(Ticket.REPORTNUMBER);
	KeyValueType additionalData4 = new KeyValueType();
	additionalData4.setKey(Ticket.INDICATOR);
	KeyValueType additionalData5 = new KeyValueType();
	additionalData5.setKey(Ticket.DATEMASSBREAKDOWN);
	KeyValueType additionalData6 = new KeyValueType();
	additionalData6.setKey(Ticket.SCHEDULEDWORKCODE);
	KeyValueType additionalData7 = new KeyValueType();
	additionalData7.setKey(Ticket.SCHEDULEDWORKDATE);
	KeyValueType additionalData8 = new KeyValueType();
	additionalData8.setKey(Ticket.SCHEDULEDWORKENDTIME);
	List<KeyValueType> additionalData = Arrays.asList(additionalData1, additionalData2, additionalData3, additionalData4,
		additionalData5, additionalData6, additionalData7, additionalData8);

	String id = cms.getFailureCMSId();
	StringBuilder description = new StringBuilder();
	description.append(cms.getFailureReasonCode()).append(Ticket.HYPHEN).append(cms.getFailureReasonDescription());

	ticket.setId(id);
	ticket.setHref(Ticket.HREF.concat(id));
	ticket.setDescription(description.toString());
	ticket.setCustomerId(cms.getClientCode());
	ticket.setAccountId(cms.getAccountCode());
	ticket.setCreationDate(Util.getOffsetDateTime(cms.getFailureDate()));
	ticket.setType(Ticket.BREAKDOWN);
	ticket.setSource(Ticket.CMS);
	if(cms.getFailureStatusDescription() != null) {
	ticket.setStatus(StatusEnum.fromValue(StatusFactory.fromCMS(cms.getFailureStatusDescription())));}
	ticket.setSubStatus(cms.getFailureStatusDescription());
	ticket.setStatusChangeDate(Util.getOffsetDateTime(cms.getFailureLastChangeDate()));
	ticket.setStatusChangeChannel(statusChangeChannel);
	ticket.setResolutionDate(Util.getOffsetDateTime(cms.getResolutionDate()));// --
	ticket.setResolutionChannel(resolutionChannel);
	ticket.setResolution(resolution.toString());// ----
	ticket.setRelatedParty(relatedParty);
	ticket.setRelatedObject(relatedObject);
	ticket.setAdditionalData(additionalData);
	ticket.setNote(note);
	ticket.setChannel(channel);

	return ticket;
    }

    public List<TicketDetailType> createTicketDetailTypeListFromGestel(String relatedObjectReference) {
	return gestelRepository.findByTelephoneNumberAndFailureDateAfter(relatedObjectReference, Util.threeMonthsAgo()).stream()
		.map(this::toTicketDetailTypeFromGestel).collect(Collectors.toList());
    }

    private TicketDetailType toTicketDetailTypeFromGestel(Gestel gestel) {
	TicketDetailType ticket = new TicketDetailType();

	ChannelRefType statusChangeChannel = new ChannelRefType();
	statusChangeChannel.setId(gestel.getFailureLastChangeUserChannel());
	statusChangeChannel.setName(gestel.getFailureLastChangeUserChannel());

	StringBuilder resolution = new StringBuilder();
	resolution.append(gestel.getResolutionCode()).append(Ticket.HYPHEN).append(gestel.getResolutionDescription());
	resolution.append(Ticket.VERTICALLINE);
	resolution.append(gestel.getResolutionReasonCode()).append(Ticket.HYPHEN).append(gestel.getResolutionReasonDescription());

	ChannelRefType resolutionChannel = new ChannelRefType();
	resolutionChannel.setId(gestel.getResolutionUserChannel());
	resolutionChannel.setName(gestel.getResolutionUserChannel());

	RelatedPartyType relatedParty1 = new RelatedPartyType();
	relatedParty1.setRole(Ticket.CREATIONUSER);
	relatedParty1.setId(gestel.getFailureRegisteredUser());
	RelatedPartyType relatedParty2 = new RelatedPartyType();
	relatedParty2.setRole(Ticket.SATUSCHANGEUSER);
	relatedParty2.setId(gestel.getFailureLastChangeUser());
	RelatedPartyType relatedParty3 = new RelatedPartyType();
	relatedParty3.setRole(Ticket.RESOLUTIONUSER);
	relatedParty3.setId(gestel.getResolutionUser());
	List<RelatedPartyType> relatedParty = Arrays.asList(relatedParty1, relatedParty2, relatedParty3);

	RelatedObjectType relatedObject1 = new RelatedObjectType();
	relatedObject1.setInvolvement(InvolvementEnum.MOBILE);
	RelatedObjectType relatedObject2 = new RelatedObjectType();
	relatedObject2.setInvolvement(InvolvementEnum.LANDLINE);
	relatedObject2.setReference(gestel.getTelephoneNumber());
	RelatedObjectType relatedObject3 = new RelatedObjectType();
	relatedObject3.setInvolvement(InvolvementEnum.CABLETV);
	relatedObject3.setReference(gestel.getTvServiceCode());
	RelatedObjectType relatedObject4 = new RelatedObjectType();
	relatedObject4.setInvolvement(InvolvementEnum.BROADBAND);
	RelatedObjectType relatedObject5 = new RelatedObjectType();
	relatedObject5.setInvolvement(InvolvementEnum.BILL);
	List<RelatedObjectType> relatedObject = Arrays.asList(relatedObject1, relatedObject2, relatedObject3, relatedObject4,
		relatedObject5);

	TicketNoteInfoType note1 = new TicketNoteInfoType();
	note1.setDate(Util.getOffsetDateTime(gestel.getFailureDate()));
	note1.setAuthor(gestel.getFailureRegisteredUser());
	note1.setText(gestel.getFailureRemark());
	TicketNoteInfoType note2 = new TicketNoteInfoType();
	note2.setDate(Util.getOffsetDateTime(gestel.getResolutionDate()));
	note2.setAuthor(gestel.getResolutionUser());
	note2.setText(gestel.getResolutionUserRemark());
	List<TicketNoteInfoType> note = Arrays.asList(note1, note2);

	ChannelRefType channel = new ChannelRefType();
	channel.setId(gestel.getFailureRegisteredChannel());
	channel.setName(gestel.getFailureRegisteredChannel());

	KeyValueType additionalData1 = new KeyValueType();
	additionalData1.setKey(Ticket.INSCRIPTION);
	additionalData1.setValue(gestel.getInscriptionNumber());
	KeyValueType additionalData2 = new KeyValueType();
	additionalData2.setKey(Ticket.CLAIMNUMBER);
	KeyValueType additionalData3 = new KeyValueType();
	additionalData3.setKey(Ticket.REPORTNUMBER);
	KeyValueType additionalData4 = new KeyValueType();
	additionalData4.setKey(Ticket.INDICATOR);
	KeyValueType additionalData5 = new KeyValueType();
	additionalData5.setKey(Ticket.DATEMASSBREAKDOWN);
	KeyValueType additionalData6 = new KeyValueType();
	additionalData6.setKey(Ticket.SCHEDULEDWORKCODE);
	KeyValueType additionalData7 = new KeyValueType();
	additionalData7.setKey(Ticket.SCHEDULEDWORKDATE);
	KeyValueType additionalData8 = new KeyValueType();
	additionalData8.setKey(Ticket.SCHEDULEDWORKENDTIME);
	List<KeyValueType> additionalData = Arrays.asList(additionalData1, additionalData2, additionalData3, additionalData4,
		additionalData5, additionalData6, additionalData7, additionalData8);

	String id = gestel.getFailureGestelId();
	StringBuilder description = new StringBuilder();
	description.append(gestel.getFailureReasonCode()).append(Ticket.HYPHEN).append(gestel.getFailureReasonDescription());

	ticket.setId(id);
	ticket.setHref(Ticket.HREF.concat(id));
	ticket.setDescription(description.toString());
	ticket.setCustomerId(gestel.getClientCode());
	ticket.setAccountId(gestel.getAccountCode());
	ticket.setCreationDate(Util.getOffsetDateTime(gestel.getFailureDate()));
	ticket.setType(Ticket.BREAKDOWN);
	ticket.setSource(Ticket.GESTEL);
	if(gestel.getFailureStatusDescription() != null) {
	ticket.setStatus(StatusEnum.fromValue(StatusFactory.fromGestel(gestel.getFailureStatusDescription())));
	}
	ticket.setSubStatus(gestel.getFailureStatusDescription());
	ticket.setStatusChangeDate(Util.getOffsetDateTime(gestel.getFailureLastChangeDate()));
	ticket.setStatusChangeChannel(statusChangeChannel);
	ticket.setResolutionDate(Util.getOffsetDateTime(gestel.getResolutionDate()));// --
	ticket.setResolutionChannel(resolutionChannel);
	ticket.setResolution(resolution.toString());// ----
	ticket.setRelatedParty(relatedParty);
	ticket.setRelatedObject(relatedObject);
	ticket.setAdditionalData(additionalData);
	ticket.setNote(note);
	ticket.setChannel(channel);

	return ticket;
    }

}
