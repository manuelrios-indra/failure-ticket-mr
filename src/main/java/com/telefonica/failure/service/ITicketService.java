package com.telefonica.failure.service;

import java.util.List;

import com.telefonica.failure.entity.ALDM;
import com.telefonica.failure.entity.CMS;
import com.telefonica.failure.entity.Gestel;
import com.telefonica.failure.types.TicketDetailType;

/**
 * 
 * @Author:
 * @Datecreation: apr. 2020
 * @FileName: ITicketService.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Interfaz para TicketService.
 */
public interface ITicketService {
	
	
	/**
	 * Metodo que obtiene todos los tickes de averias
	 * @param type
	 * @param relatedObjectInvolvement
	 * @param relatedObjectReference
	 * @return List<TicketDetailType>
	 */
	public List<TicketDetailType> retrieveTickets(String type, String relatedObjectInvolvement, String relatedObjectReference);
	
	/**
	 * Obtiene una lista de tickets desde la tabla AVERIAS_ALDM
	 * @param relatedObjectReference
	 * @return List<TicketDetailType>
	 */
	public List<TicketDetailType> createTicketDetailTypeListFromALDM(String relatedObjectReference);
	
	/**
	 * Obtiene una lista de tickets desde la tabla AVERIAS_CMS
	 * @param relatedObjectReference
	 * @return List<TicketDetailType>
	 */
	public List<TicketDetailType> createTicketDetailTypeListFromCMS(String relatedObjectReference);
	
	/**
	 * Obtiene una lista de tickets desde la tabla AVERIAS_GESTEL
	 * @param relatedObjectReference
	 * @return List<TicketDetailType>
	 */
	public List<TicketDetailType> createTicketDetailTypeListFromGestel(String relatedObjectReference);
	
	
	

}
