package com.telefonica.failure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class FailureTicketApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
	SpringApplication.run(FailureTicketApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	return application.sources(FailureTicketApplication.class);
    }

}
