package com.telefonica.failure.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @Author:
 * @Datecreation:
 * @FileName: BusinessException.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Clase exception.
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class BusinessException extends RuntimeException {

    private final String businessError;

    private static final long serialVersionUID = -803774844847541913L;

    public BusinessException(String businessError) {
	this.businessError = businessError;
    }

    public BusinessException(Throwable cause, String businessError) {
	super(cause);
	this.businessError = businessError;
    }

}
