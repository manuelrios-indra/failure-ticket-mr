package com.telefonica.failure.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.failure.service.ITicketService;
import com.telefonica.failure.types.ExceptionType;
import com.telefonica.failure.types.TicketDetailType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "tickets", consumes = "application/json")
@RestController
@RequestMapping(value = "/tickets")
public class TicketsApiController {

    @Autowired
    private ITicketService ticketService;

    @ApiOperation(value = "Retrieve a list of failure tickets", nickname = "retrieveTickets", notes = "", response = TicketDetailType.class, responseContainer = "List", tags = {
	    "tickets", })
    @ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Tickets retrieved successfully", response = TicketDetailType.class, responseContainer = "List"),
	    @ApiResponse(code = 400, message = "Client Error", response = ExceptionType.class),
	    @ApiResponse(code = 404, message = "Not Found Error", response = ExceptionType.class),
	    @ApiResponse(code = 500, message = "Server Error", response = ExceptionType.class),
	    @ApiResponse(code = 503, message = "Service Unavailable", response = ExceptionType.class) })
    @GetMapping(produces = { "application/json" }, value = "/retrieveTickets")
    public ResponseEntity<List<TicketDetailType>> retrieveTickets(
	    @ApiParam(value = "If this API is used via a platform acting as a common entry point to different OBs, this identifier is used to route the request to the corresponding OB environment") @RequestHeader(value = "UNICA-ServiceId", required = false) String unICAServiceId,
	    @ApiParam(value = "Identifier for the system originating the request") @RequestHeader(value = "UNICA-Application", required = false) String unICAApplication,
	    @ApiParam(value = "Unique identifier for the process or execution flow") @RequestHeader(value = "UNICA-PID", required = false) String UNICA_PID,
	    @ApiParam(value = "Identifies the user when the request is received from a trusted application and no end user authorization token is used but just an application token") @RequestHeader(value = "UNICA-User", required = false) String unICAUser,
	    @ApiParam(value = "Including the proof of access (using OAuth2.0 security model) to guarantee that the consumer has privileges to access the entity database") @RequestHeader(value = "Authorization", required = false) String authorization,
	    @ApiParam(value = "Migration indicator.") @RequestHeader(value = "migrationIndicator", required = false) String migrationIndicator,
	    @ApiParam(value = "Origin system.") @RequestHeader(value = "originSystem", required = false) String originSystem,
	    @ApiParam(value = "To obtain the list of tickets stored in the server that are of a given type") @Valid @RequestParam(value = "type", required = false) String type,
	    @ApiParam(value = "To obtain the list of tickets stored in the server that are related to an specific type of object") @Valid @RequestParam(value = "relatedObject.involvement", required = false) String relatedObjectInvolvement,
	    @ApiParam(value = "To obtain the list of tickets stored in the server that are related to an specific object entity") @Valid @RequestParam(value = "relatedObject.reference", required = false) String relatedObjectReference,
	    @ApiParam(value = "To obtain tickets related to whis national ID value. Must be used together with nationalIdType") @Valid @RequestParam(value = "nationalId", required = false) String nationalId) {

	HttpHeaders httpHeaders = new HttpHeaders();
	httpHeaders.add("UNICA-ServiceId", unICAServiceId);
	httpHeaders.add("UNICA-PID", UNICA_PID);
	
	List<TicketDetailType> ticketsList = ticketService.retrieveTickets(type, relatedObjectInvolvement, relatedObjectReference);
	return new ResponseEntity<List<TicketDetailType>>(ticketsList, httpHeaders, HttpStatus.OK);

    }

}
