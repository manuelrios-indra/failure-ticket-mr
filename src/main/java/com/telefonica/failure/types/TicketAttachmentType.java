package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.TicketAttachmentSummaryType;

import io.swagger.annotations.ApiModelProperty;

import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketAttachmentType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketAttachmentType   {
  @JsonProperty("attachmentSummary")
  private TicketAttachmentSummaryType attachmentSummary = null;

  @JsonProperty("content")
  private String content = null;

  public TicketAttachmentType attachmentSummary(TicketAttachmentSummaryType attachmentSummary) {
    this.attachmentSummary = attachmentSummary;
    return this;
  }

  /**
   * Data describing the attachment
   * @return attachmentSummary
  **/
  @ApiModelProperty(required = true, value = "Data describing the attachment")
  @NotNull

  @Valid

  public TicketAttachmentSummaryType getAttachmentSummary() {
    return attachmentSummary;
  }

  public void setAttachmentSummary(TicketAttachmentSummaryType attachmentSummary) {
    this.attachmentSummary = attachmentSummary;
  }

  public TicketAttachmentType content(String content) {
    this.content = content;
    return this;
  }

  /**
   * Actual content of the attachment. Note it has to be encoded in base64 in order to be compatible with JSON representation
   * @return content
  **/
  @ApiModelProperty(required = true, value = "Actual content of the attachment. Note it has to be encoded in base64 in order to be compatible with JSON representation")
  @NotNull


  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketAttachmentType ticketAttachmentType = (TicketAttachmentType) o;
    return Objects.equals(this.attachmentSummary, ticketAttachmentType.attachmentSummary) &&
        Objects.equals(this.content, ticketAttachmentType.content);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attachmentSummary, content);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketAttachmentType {\n");
    
    sb.append("    attachmentSummary: ").append(toIndentedString(attachmentSummary)).append("\n");
    sb.append("    content: ").append(toIndentedString(content)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

