package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.CategoryTreeType;

import io.swagger.annotations.ApiModelProperty;

import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * CategoryTreeType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class CategoryTreeType   {
    
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  public CategoryTreeType id(String id) {
    this.id = id;
    return this;
  }

  /**
   * An identifier for the category
   * @return id
  **/
  @ApiModelProperty(required = true, value = "An identifier for the category")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public CategoryTreeType name(String name) {
    this.name = name;
    return this;
  }

  /**
   * A human readable category name
   * @return name
  **/
  @ApiModelProperty(value = "A human readable category name")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CategoryTreeType categoryTreeType = (CategoryTreeType) o;
    return Objects.equals(this.id, categoryTreeType.id) &&
        Objects.equals(this.name, categoryTreeType.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CategoryTreeType {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

