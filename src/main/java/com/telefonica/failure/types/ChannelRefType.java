package com.telefonica.failure.types;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * Reference to a channel that can be queried with an API call.
 */
@ApiModel(description = "Reference to a channel that can be queried with an API call.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class ChannelRefType {

    @JsonProperty("id")
    private String id = null;

    @JsonProperty("name")
    private String name = null;

    /**
     * Contents of the note (comment) to be associated to the ticket
     * 
     * @return text
     **/
    @ApiModelProperty(required = true, value = "Date of the registered failure or claim")
    @NotNull

    public ChannelRefType id(String id) {
	this.id = id;
	return this;
    }

    /**
     * Unique identifier of the channel
     * 
     * @return id
     **/
    @ApiModelProperty(required = true, value = "Unique identifier of the channel")
    @NotNull

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public ChannelRefType name(String name) {
	this.name = name;
	return this;
    }

    /**
     * Screen name of the channel
     * 
     * @return name
     **/
    @ApiModelProperty(value = "Screen name of the channel")

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((name == null) ? 0 : name.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	ChannelRefType other = (ChannelRefType) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	if (name == null) {
	    if (other.name != null)
		return false;
	} else if (!name.equals(other.name))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "ChannelRefType [id=" + id + ", name=" + name + "]";
    }

}