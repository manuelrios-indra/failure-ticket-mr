package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * TicketAttachmentSummaryType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketAttachmentSummaryType   {
  @JsonProperty("contentId")
  private String contentId = null;

  @JsonProperty("contentLength")
  private Integer contentLength = null;

  @JsonProperty("contentType")
  private String contentType = null;

  @JsonProperty("name")
  private String name = null;

  public TicketAttachmentSummaryType contentId(String contentId) {
    this.contentId = contentId;
    return this;
  }

  /**
   * Unique identifier within the client of the content provided. This can be used to correlate attachmentId and contentId
   * @return contentId
  **/
  @ApiModelProperty(value = "Unique identifier within the client of the content provided. This can be used to correlate attachmentId and contentId")


  public String getContentId() {
    return contentId;
  }

  public void setContentId(String contentId) {
    this.contentId = contentId;
  }

  public TicketAttachmentSummaryType contentLength(Integer contentLength) {
    this.contentLength = contentLength;
    return this;
  }

  /**
   * Length in bytes of the content
   * @return contentLength
  **/
  @ApiModelProperty(value = "Length in bytes of the content")


  public Integer getContentLength() {
    return contentLength;
  }

  public void setContentLength(Integer contentLength) {
    this.contentLength = contentLength;
  }

  public TicketAttachmentSummaryType contentType(String contentType) {
    this.contentType = contentType;
    return this;
  }

  /**
   * Internet media type of the attachment content (e.g. application/pdf, text/plain, etc)
   * @return contentType
  **/
  @ApiModelProperty(required = true, value = "Internet media type of the attachment content (e.g. application/pdf, text/plain, etc)")
  @NotNull


  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public TicketAttachmentSummaryType name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the attached file
   * @return name
  **/
  @ApiModelProperty(required = true, value = "Name of the attached file")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketAttachmentSummaryType ticketAttachmentSummaryType = (TicketAttachmentSummaryType) o;
    return Objects.equals(this.contentId, ticketAttachmentSummaryType.contentId) &&
        Objects.equals(this.contentLength, ticketAttachmentSummaryType.contentLength) &&
        Objects.equals(this.contentType, ticketAttachmentSummaryType.contentType) &&
        Objects.equals(this.name, ticketAttachmentSummaryType.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contentId, contentLength, contentType, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketAttachmentSummaryType {\n");
    
    sb.append("    contentId: ").append(toIndentedString(contentId)).append("\n");
    sb.append("    contentLength: ").append(toIndentedString(contentLength)).append("\n");
    sb.append("    contentType: ").append(toIndentedString(contentType)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

