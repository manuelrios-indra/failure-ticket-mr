package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.telefonica.failure.types.ChannelRefType;

import io.swagger.annotations.ApiModelProperty;

import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketStatusHistoryItemType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketStatusHistoryItemType   {
  @JsonProperty("statusChangeDate")
  private OffsetDateTime statusChangeDate = null;

  @JsonProperty("statusChangeReason")
  private String statusChangeReason = null;

  @JsonProperty("statusChangeChannel")
  private ChannelRefType statusChangeChannel = null;

  /**
   * Status of the ticket
   */
  public enum TicketStatusEnum {
    NEW("new"),
    
    SUBMITTED("submitted"),
    
    ACKNOWLEDGED("acknowledged"),
    
    IN_PROGRESS("in progress"),
    
    RESOLVED("resolved"),
    
    CLOSED("closed"),
    
    REOPEN("reopen"),
    
    CANCELLED("cancelled"),
    
    REJECTED("rejected"),
    
    PENDING("pending"),
    
    ASSIGNED("assigned"),
    
    HELD("held");

    private String value;

    TicketStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TicketStatusEnum fromValue(String text) {
      for (TicketStatusEnum b : TicketStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("ticketStatus")
  private TicketStatusEnum ticketStatus = null;

  @JsonProperty("ticketSubstatus")
  private String ticketSubstatus = null;

  public TicketStatusHistoryItemType statusChangeDate(OffsetDateTime statusChangeDate) {
    this.statusChangeDate = statusChangeDate;
    return this;
  }

  /**
   * Date registered for the last change of status or substatus
   * @return statusChangeDate
  **/
  @ApiModelProperty(required = true, value = "Date registered for the last change of status or substatus")
  @NotNull

  @Valid

  public OffsetDateTime getStatusChangeDate() {
    return statusChangeDate;
  }

  public void setStatusChangeDate(OffsetDateTime statusChangeDate) {
    this.statusChangeDate = statusChangeDate;
  }

  public TicketStatusHistoryItemType statusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
    return this;
  }

  /**
   * Reasoning registered for the last change of status or susbstatus
   * @return statusChangeReason
  **/
  @ApiModelProperty(value = "Reasoning registered for the last change of status or susbstatus")


  public String getStatusChangeReason() {
    return statusChangeReason;
  }

  public void setStatusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
  }

  public TicketStatusHistoryItemType statusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
    return this;
  }

  /**
   * channel where the status was last changed
   * @return statusChangeChannel
  **/
  @ApiModelProperty(value = "channel where the status was last changed")

  @Valid

  public ChannelRefType getStatusChangeChannel() {
    return statusChangeChannel;
  }

  public void setStatusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
  }

  public TicketStatusHistoryItemType ticketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
    return this;
  }

  /**
   * Status of the ticket
   * @return ticketStatus
  **/
  @ApiModelProperty(required = true, value = "Status of the ticket")
  @NotNull


  public TicketStatusEnum getTicketStatus() {
    return ticketStatus;
  }

  public void setTicketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
  }

  public TicketStatusHistoryItemType ticketSubstatus(String ticketSubstatus) {
    this.ticketSubstatus = ticketSubstatus;
    return this;
  }

  /**
   * Substatus in order to define a second status level
   * @return ticketSubstatus
  **/
  @ApiModelProperty(value = "Substatus in order to define a second status level")


  public String getTicketSubstatus() {
    return ticketSubstatus;
  }

  public void setTicketSubstatus(String ticketSubstatus) {
    this.ticketSubstatus = ticketSubstatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketStatusHistoryItemType ticketStatusHistoryItemType = (TicketStatusHistoryItemType) o;
    return Objects.equals(this.statusChangeDate, ticketStatusHistoryItemType.statusChangeDate) &&
        Objects.equals(this.statusChangeReason, ticketStatusHistoryItemType.statusChangeReason) &&
        Objects.equals(this.statusChangeChannel, ticketStatusHistoryItemType.statusChangeChannel) &&
        Objects.equals(this.ticketStatus, ticketStatusHistoryItemType.ticketStatus) &&
        Objects.equals(this.ticketSubstatus, ticketStatusHistoryItemType.ticketSubstatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusChangeDate, statusChangeReason, statusChangeChannel, ticketStatus, ticketSubstatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketStatusHistoryItemType {\n");
    
    sb.append("    statusChangeDate: ").append(toIndentedString(statusChangeDate)).append("\n");
    sb.append("    statusChangeReason: ").append(toIndentedString(statusChangeReason)).append("\n");
    sb.append("    statusChangeChannel: ").append(toIndentedString(statusChangeChannel)).append("\n");
    sb.append("    ticketStatus: ").append(toIndentedString(ticketStatus)).append("\n");
    sb.append("    ticketSubstatus: ").append(toIndentedString(ticketSubstatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

