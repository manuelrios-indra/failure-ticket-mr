package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.telefonica.failure.types.KeyValueType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * RelatedObjectType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class RelatedObjectType   {
  /**
   * Indication of the relationship defined between the object and the ticket reported (e.g.: disputed invoice, adjusted invoice, related product,�). Supported values are implementation and application specific
   */
  public enum InvolvementEnum {
    MOBILE("mobile"),
    
    LANDLINE("landline"),
    
    IPTV("ipTv"),
    
    CABLETV("cableTv"),
    
    EMAIL("email"),
    
    BROADBAND("broadband"),
    
    PRODUCT("product"),
    
    BILL("bill");

    private String value;

    InvolvementEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static InvolvementEnum fromValue(String text) {
      for (InvolvementEnum b : InvolvementEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("involvement")
  private InvolvementEnum involvement = null;

  @JsonProperty("reference")
  private String reference = null;

  @JsonProperty("additionalData")
  @Valid
  private List<KeyValueType> additionalData = null;

  public RelatedObjectType involvement(InvolvementEnum involvement) {
    this.involvement = involvement;
    return this;
  }

  /**
   * Indication of the relationship defined between the object and the ticket reported (e.g.: disputed invoice, adjusted invoice, related product,�). Supported values are implementation and application specific
   * @return involvement
  **/
  @ApiModelProperty(value = "Indication of the relationship defined between the object and the ticket reported (e.g.: disputed invoice, adjusted invoice, related product,�). Supported values are implementation and application specific")


  public InvolvementEnum getInvolvement() {
    return involvement;
  }

  public void setInvolvement(InvolvementEnum involvement) {
    this.involvement = involvement;
  }

  public RelatedObjectType reference(String reference) {
    this.reference = reference;
    return this;
  }

  /**
   * String providing identification of the related object reported
   * @return reference
  **/
  @ApiModelProperty(required = true, value = "String providing identification of the related object reported")
  @NotNull


  public String getReference() {
    return reference;
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public RelatedObjectType additionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  public RelatedObjectType addAdditionalDataItem(KeyValueType additionalDataItem) {
    if (this.additionalData == null) {
      this.additionalData = new ArrayList<KeyValueType>();
    }
    this.additionalData.add(additionalDataItem);
    return this;
  }

  /**
   * Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs
   * @return additionalData
  **/
  @ApiModelProperty(value = "Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs")

  @Valid

  public List<KeyValueType> getAdditionalData() {
    return additionalData;
  }

  public void setAdditionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelatedObjectType relatedObjectType = (RelatedObjectType) o;
    return Objects.equals(this.involvement, relatedObjectType.involvement) &&
        Objects.equals(this.reference, relatedObjectType.reference) &&
        Objects.equals(this.additionalData, relatedObjectType.additionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(involvement, reference, additionalData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelatedObjectType {\n");
    
    sb.append("    involvement: ").append(toIndentedString(involvement)).append("\n");
    sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

