package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * RelatedPartyType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class RelatedPartyType   {
  @JsonProperty("role")
  private String role = null;

  @JsonProperty("id")
  private String id = null;

  public RelatedPartyType role(String role) {
    this.role = role;
    return this;
  }

  /**
   * Indication of the relationship defined between the individual and the ticket reported (e.g.: originator, system impacted, reviewer, �). Supported values are implementation and application specific
   * @return role
  **/
  @ApiModelProperty(value = "Indication of the relationship defined between the individual and the ticket reported (e.g.: originator, system impacted, reviewer, �). Supported values are implementation and application specific")


  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public RelatedPartyType id(String id) {
    this.id = id;
    return this;
  }

  /**
   * String providing identification of the related party reported
   * @return id
  **/
  @ApiModelProperty(required = true, value = "String providing identification of the related party reported")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RelatedPartyType relatedPartyType = (RelatedPartyType) o;
    return Objects.equals(this.role, relatedPartyType.role) &&
        Objects.equals(this.id, relatedPartyType.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(role, id);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RelatedPartyType {\n");
    
    sb.append("    role: ").append(toIndentedString(role)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

