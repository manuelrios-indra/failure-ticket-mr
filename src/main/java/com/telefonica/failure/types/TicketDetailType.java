package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.telefonica.failure.types.CategoryTreeType;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.RelatedObjectType;
import com.telefonica.failure.types.RelatedPartyType;
import com.telefonica.failure.types.TicketNoteInfoType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketDetailType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketDetailType   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("href")
  private String href = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("customerId")
  private String customerId = null;

  @JsonProperty("accountId")
  private String accountId = null;

  @JsonProperty("creationDate")
  private OffsetDateTime creationDate = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("category")
  private CategoryTreeType category = null;

  @JsonProperty("source")
  private String source = null;

  @JsonProperty("parentTicket")
  private String parentTicket = null;

  /**
   * Status of the ticket
   */
  public enum StatusEnum {
    NEW("new"),
    
    SUBMITTED("submitted"),
    
    ACKNOWLEDGED("acknowledged"),
    
    IN_PROGRESS("in progress"),
    
    RESOLVED("resolved"),
    
    CLOSED("closed"),
    
    REOPEN("reopen"),
    
    CANCELLED("cancelled"),
    
    REJECTED("rejected"),
    
    PENDING("pending"),
    
    ASSIGNED("assigned"),
    
    HELD("held");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String text) {
      for (StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("subStatus")
  private String subStatus = null;

  @JsonProperty("statusChangeDate")
  private OffsetDateTime statusChangeDate = null;

  @JsonProperty("statusChangeChannel")
  private ChannelRefType statusChangeChannel = null;

  @JsonProperty("resolutionDate")
  private OffsetDateTime resolutionDate = null;

  @JsonProperty("resolutionChannel")
  private ChannelRefType resolutionChannel = null;

  @JsonProperty("resolution")
  private String resolution = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyType> relatedParty = null;

  @JsonProperty("relatedObject")
  @Valid
  private List<RelatedObjectType> relatedObject = null;

  @JsonProperty("note")
  @Valid
  private List<TicketNoteInfoType> note = null;

  @JsonProperty("channel")
  private ChannelRefType channel = null;

  @JsonProperty("additionalData")
  @Valid
  private List<KeyValueType> additionalData = null;

  public TicketDetailType id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Unique Identifier within the server for the ticket reported
   * @return id
  **/
  @ApiModelProperty(required = true, value = "Unique Identifier within the server for the ticket reported")
  @NotNull


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public TicketDetailType href(String href) {
    this.href = href;
    return this;
  }

  /**
   * A resource URI pointing to the resource in the OB that stores the ticket detailed information
   * @return href
  **/
  @ApiModelProperty(required = true, value = "A resource URI pointing to the resource in the OB that stores the ticket detailed information")
  @NotNull


  public String getHref() {
    return href;
  }

  public void setHref(String href) {
    this.href = href;
  }

  public TicketDetailType description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Some text providing a user-friendly detailed description of the ticket raised
   * @return description
  **/
  @ApiModelProperty(required = true, value = "Some text providing a user-friendly detailed description of the ticket raised")
  @NotNull


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TicketDetailType customerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

  /**
   * Unique Identifier for the customer originating the ticket (e.g.: OB customer number)
   * @return customerId
  **/
  @ApiModelProperty(value = "Unique Identifier for the customer originating the ticket (e.g.: OB customer number)")


  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public TicketDetailType accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

  /**
   * Unique Identifier for the account within the server to be linked to the ticket (e.g.: customer account number)
   * @return accountId
  **/
  @ApiModelProperty(value = "Unique Identifier for the account within the server to be linked to the ticket (e.g.: customer account number)")


  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public TicketDetailType creationDate(OffsetDateTime creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  /**
   * Date when the ticket was created in the server
   * @return creationDate
  **/
  @ApiModelProperty(required = true, value = "Date when the ticket was created in the server")
  @NotNull

  @Valid

  public OffsetDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(OffsetDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public TicketDetailType type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Indication of the type of ticket registered. Supported values are implementation and application specific
   * @return type
  **/
  @ApiModelProperty(required = true, value = "Indication of the type of ticket registered. Supported values are implementation and application specific")
  @NotNull


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public TicketDetailType category(CategoryTreeType category) {
    this.category = category;
    return this;
  }

  /**
   * List of categories/subcategories allocated to the ticket, intended to allow segmentation
   * @return category
  **/
  @ApiModelProperty(value = "List of categories/subcategories allocated to the ticket, intended to allow segmentation")

  @Valid

  public CategoryTreeType getCategory() {
    return category;
  }

  public void setCategory(CategoryTreeType category) {
    this.category = category;
  }

  public TicketDetailType source(String source) {
    this.source = source;
    return this;
  }

  /**
   * Indicates the origin of the ticket. Supported values are implementation and application specific
   * @return source
  **/
  @ApiModelProperty(value = "Indicates the origin of the ticket. Supported values are implementation and application specific")


  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public TicketDetailType parentTicket(String parentTicket) {
    this.parentTicket = parentTicket;
    return this;
  }

  /**
   * Unique Identifier for another ticket within the server associated to the reported one
   * @return parentTicket
  **/
  @ApiModelProperty(value = "Unique Identifier for another ticket within the server associated to the reported one")


  public String getParentTicket() {
    return parentTicket;
  }

  public void setParentTicket(String parentTicket) {
    this.parentTicket = parentTicket;
  }

  public TicketDetailType status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Status of the ticket
   * @return status
  **/
  @ApiModelProperty(required = true, value = "Status of the ticket")
  @NotNull


  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public TicketDetailType subStatus(String subStatus) {
    this.subStatus = subStatus;
    return this;
  }

  /**
   * Substatus in order to define a second status level
   * @return subStatus
  **/
  @ApiModelProperty(value = "Substatus in order to define a second status level")


  public String getSubStatus() {
    return subStatus;
  }

  public void setSubStatus(String subStatus) {
    this.subStatus = subStatus;
  }

  public TicketDetailType statusChangeDate(OffsetDateTime statusChangeDate) {
    this.statusChangeDate = statusChangeDate;
    return this;
  }

  /**
   * Date registered for the last change of status
   * @return statusChangeDate
  **/
  @ApiModelProperty(value = "Date registered for the last change of status")

  @Valid

  public OffsetDateTime getStatusChangeDate() {
    return statusChangeDate;
  }

  public void setStatusChangeDate(OffsetDateTime statusChangeDate) {
    this.statusChangeDate = statusChangeDate;
  }

  public TicketDetailType statusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
    return this;
  }

  /**
   * Channel where the status was last changed
   * @return statusChangeChannel
  **/
  @ApiModelProperty(value = "Channel where the status was last changed")

  @Valid

  public ChannelRefType getStatusChangeChannel() {
    return statusChangeChannel;
  }

  public void setStatusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
  }

  public TicketDetailType resolutionDate(OffsetDateTime resolutionDate) {
    this.resolutionDate = resolutionDate;
    return this;
  }

  /**
   * Date registered as the actual resolution of the ticket
   * @return resolutionDate
  **/
  @ApiModelProperty(value = "Date registered as the actual resolution of the ticket")

  @Valid

  public OffsetDateTime getResolutionDate() {
    return resolutionDate;
  }

  public void setResolutionDate(OffsetDateTime resolutionDate) {
    this.resolutionDate = resolutionDate;
  }

  public TicketDetailType resolutionChannel(ChannelRefType resolutionChannel) {
    this.resolutionChannel = resolutionChannel;
    return this;
  }

  /**
   * Channel where the ticket was resolved
   * @return resolutionChannel
  **/
  @ApiModelProperty(value = "Channel where the ticket was resolved")

  @Valid

  public ChannelRefType getResolutionChannel() {
    return resolutionChannel;
  }

  public void setResolutionChannel(ChannelRefType resolutionChannel) {
    this.resolutionChannel = resolutionChannel;
  }

  public TicketDetailType resolution(String resolution) {
    this.resolution = resolution;
    return this;
  }

  /**
   * Reasoning registered for the resolution of the ticket
   * @return resolution
  **/
  @ApiModelProperty(value = "Reasoning registered for the resolution of the ticket")


  public String getResolution() {
    return resolution;
  }

  public void setResolution(String resolution) {
    this.resolution = resolution;
  }

  public TicketDetailType relatedParty(List<RelatedPartyType> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public TicketDetailType addRelatedPartyItem(RelatedPartyType relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyType>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * List of individuals (e.g.: originator, system impacted, reviewer, �) associated to a ticket
   * @return relatedParty
  **/
  @ApiModelProperty(value = "List of individuals (e.g.: originator, system impacted, reviewer, �) associated to a ticket")

  @Valid

  public List<RelatedPartyType> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyType> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public TicketDetailType relatedObject(List<RelatedObjectType> relatedObject) {
    this.relatedObject = relatedObject;
    return this;
  }

  public TicketDetailType addRelatedObjectItem(RelatedObjectType relatedObjectItem) {
    if (this.relatedObject == null) {
      this.relatedObject = new ArrayList<RelatedObjectType>();
    }
    this.relatedObject.add(relatedObjectItem);
    return this;
  }

  /**
   * List of Objects or resources (e.g.: invoices, products, payments, �) associated to a ticket
   * @return relatedObject
  **/
  @ApiModelProperty(value = "List of Objects or resources (e.g.: invoices, products, payments, �) associated to a ticket")

  @Valid

  public List<RelatedObjectType> getRelatedObject() {
    return relatedObject;
  }

  public void setRelatedObject(List<RelatedObjectType> relatedObject) {
    this.relatedObject = relatedObject;
  }

  public TicketDetailType note(List<TicketNoteInfoType> note) {
    this.note = note;
    return this;
  }

  public TicketDetailType addNoteItem(TicketNoteInfoType noteItem) {
    if (this.note == null) {
      this.note = new ArrayList<TicketNoteInfoType>();
    }
    this.note.add(noteItem);
    return this;
  }

  /**
   * List of notes to be reported associated to a ticket
   * @return note
  **/
  @ApiModelProperty(value = "List of notes to be reported associated to a ticket")

  @Valid

  public List<TicketNoteInfoType> getNote() {
    return note;
  }

  public void setNote(List<TicketNoteInfoType> note) {
    this.note = note;
  }

  public TicketDetailType channel(ChannelRefType channel) {
    this.channel = channel;
    return this;
  }

  /**
   * Reference to the channel where the ticket was raised
   * @return channel
  **/
  @ApiModelProperty(value = "Reference to the channel where the ticket was raised")

  @Valid

  public ChannelRefType getChannel() {
    return channel;
  }

  public void setChannel(ChannelRefType channel) {
    this.channel = channel;
  }

  public TicketDetailType additionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  public TicketDetailType addAdditionalDataItem(KeyValueType additionalDataItem) {
    if (this.additionalData == null) {
      this.additionalData = new ArrayList<KeyValueType>();
    }
    this.additionalData.add(additionalDataItem);
    return this;
  }

  /**
   * Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs
   * @return additionalData
  **/
  @ApiModelProperty(value = "Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs")

  @Valid

  public List<KeyValueType> getAdditionalData() {
    return additionalData;
  }

  public void setAdditionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketDetailType ticketDetailType = (TicketDetailType) o;
    return Objects.equals(this.id, ticketDetailType.id) &&
        Objects.equals(this.href, ticketDetailType.href) &&
        Objects.equals(this.description, ticketDetailType.description) &&
        Objects.equals(this.customerId, ticketDetailType.customerId) &&
        Objects.equals(this.accountId, ticketDetailType.accountId) &&
        Objects.equals(this.creationDate, ticketDetailType.creationDate) &&
        Objects.equals(this.type, ticketDetailType.type) &&
        Objects.equals(this.category, ticketDetailType.category) &&
        Objects.equals(this.source, ticketDetailType.source) &&
        Objects.equals(this.parentTicket, ticketDetailType.parentTicket) &&
        Objects.equals(this.status, ticketDetailType.status) &&
        Objects.equals(this.subStatus, ticketDetailType.subStatus) &&
        Objects.equals(this.statusChangeDate, ticketDetailType.statusChangeDate) &&
        Objects.equals(this.statusChangeChannel, ticketDetailType.statusChangeChannel) &&
        Objects.equals(this.resolutionDate, ticketDetailType.resolutionDate) &&
        Objects.equals(this.resolutionChannel, ticketDetailType.resolutionChannel) &&
        Objects.equals(this.resolution, ticketDetailType.resolution) &&
        Objects.equals(this.relatedParty, ticketDetailType.relatedParty) &&
        Objects.equals(this.relatedObject, ticketDetailType.relatedObject) &&
        Objects.equals(this.note, ticketDetailType.note) &&
        Objects.equals(this.channel, ticketDetailType.channel) &&
        Objects.equals(this.additionalData, ticketDetailType.additionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, href, description, customerId, accountId, creationDate, type, category, source, parentTicket, status, subStatus, statusChangeDate, statusChangeChannel, resolutionDate, resolutionChannel, resolution, relatedParty, relatedObject, note, channel, additionalData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketDetailType {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    href: ").append(toIndentedString(href)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    creationDate: ").append(toIndentedString(creationDate)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    parentTicket: ").append(toIndentedString(parentTicket)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    subStatus: ").append(toIndentedString(subStatus)).append("\n");
    sb.append("    statusChangeDate: ").append(toIndentedString(statusChangeDate)).append("\n");
    sb.append("    statusChangeChannel: ").append(toIndentedString(statusChangeChannel)).append("\n");
    sb.append("    resolutionDate: ").append(toIndentedString(resolutionDate)).append("\n");
    sb.append("    resolutionChannel: ").append(toIndentedString(resolutionChannel)).append("\n");
    sb.append("    resolution: ").append(toIndentedString(resolution)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    relatedObject: ").append(toIndentedString(relatedObject)).append("\n");
    sb.append("    note: ").append(toIndentedString(note)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

