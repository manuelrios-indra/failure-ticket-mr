package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.TicketAttachmentType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketNoteRequestType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketNoteRequestType   {
  @JsonProperty("author")
  private String author = null;

  @JsonProperty("text")
  private String text = null;

  @JsonProperty("attachment")
  private TicketAttachmentType attachment = null;

  @JsonProperty("channel")
  private ChannelRefType channel = null;

  @JsonProperty("additionalData")
  @Valid
  private List<KeyValueType> additionalData = null;

  public TicketNoteRequestType author(String author) {
    this.author = author;
    return this;
  }

  /**
   * Identification of the originator of the note. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal
   * @return author
  **/
  @ApiModelProperty(required = true, value = "Identification of the originator of the note. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal")
  @NotNull


  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public TicketNoteRequestType text(String text) {
    this.text = text;
    return this;
  }

  /**
   * Contents of the note (comment) to be associated to the ticket
   * @return text
  **/
  @ApiModelProperty(required = true, value = "Contents of the note (comment) to be associated to the ticket")
  @NotNull


  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public TicketNoteRequestType attachment(TicketAttachmentType attachment) {
    this.attachment = attachment;
    return this;
  }

  /**
   * List of attachments to be added as part of the creation of the note. NOTE: Including attachments within a note is not preferred and will be deprecated in future versions. The preferred way to associate attachments and notes is by means of creating independent notes and attachment entities and using an additionalData element in both structures to link them together
   * @return attachment
  **/
  @ApiModelProperty(value = "List of attachments to be added as part of the creation of the note. NOTE: Including attachments within a note is not preferred and will be deprecated in future versions. The preferred way to associate attachments and notes is by means of creating independent notes and attachment entities and using an additionalData element in both structures to link them together")

  @Valid

  public TicketAttachmentType getAttachment() {
    return attachment;
  }

  public void setAttachment(TicketAttachmentType attachment) {
    this.attachment = attachment;
  }

  public TicketNoteRequestType channel(ChannelRefType channel) {
    this.channel = channel;
    return this;
  }

  /**
   * Channel used to write the note
   * @return channel
  **/
  @ApiModelProperty(value = "Channel used to write the note")

  @Valid

  public ChannelRefType getChannel() {
    return channel;
  }

  public void setChannel(ChannelRefType channel) {
    this.channel = channel;
  }

  public TicketNoteRequestType additionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  public TicketNoteRequestType addAdditionalDataItem(KeyValueType additionalDataItem) {
    if (this.additionalData == null) {
      this.additionalData = new ArrayList<KeyValueType>();
    }
    this.additionalData.add(additionalDataItem);
    return this;
  }

  /**
   * Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs
   * @return additionalData
  **/
  @ApiModelProperty(value = "Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs")

  @Valid

  public List<KeyValueType> getAdditionalData() {
    return additionalData;
  }

  public void setAdditionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketNoteRequestType ticketNoteRequestType = (TicketNoteRequestType) o;
    return Objects.equals(this.author, ticketNoteRequestType.author) &&
        Objects.equals(this.text, ticketNoteRequestType.text) &&
        Objects.equals(this.attachment, ticketNoteRequestType.attachment) &&
        Objects.equals(this.channel, ticketNoteRequestType.channel) &&
        Objects.equals(this.additionalData, ticketNoteRequestType.additionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(author, text, attachment, channel, additionalData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketNoteRequestType {\n");
    
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    text: ").append(toIndentedString(text)).append("\n");
    sb.append("    attachment: ").append(toIndentedString(attachment)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

