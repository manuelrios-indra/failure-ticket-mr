package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.ExceptionType;

import io.swagger.annotations.ApiModelProperty;

import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ApiTransactionStatusType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class ApiTransactionStatusType   {
  @JsonProperty("error")
  private ExceptionType error = null;

  @JsonProperty("transactionStatus")
  private String transactionStatus = null;

  public ApiTransactionStatusType error(ExceptionType error) {
    this.error = error;
    return this;
  }

  /**
   * If status is Fail� this element must be used. Contains the details of the failure
   * @return error
  **/
  @ApiModelProperty(value = "If status is Fail� this element must be used. Contains the details of the failure")

  @Valid

  public ExceptionType getError() {
    return error;
  }

  public void setError(ExceptionType error) {
    this.error = error;
  }

  public ApiTransactionStatusType transactionStatus(String transactionStatus) {
    this.transactionStatus = transactionStatus;
    return this;
  }

  /**
   * Status of the transaction
   * @return transactionStatus
  **/
  @ApiModelProperty(required = true, value = "Status of the transaction")
  @NotNull


  public String getTransactionStatus() {
    return transactionStatus;
  }

  public void setTransactionStatus(String transactionStatus) {
    this.transactionStatus = transactionStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ApiTransactionStatusType apiTransactionStatusType = (ApiTransactionStatusType) o;
    return Objects.equals(this.error, apiTransactionStatusType.error) &&
        Objects.equals(this.transactionStatus, apiTransactionStatusType.transactionStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(error, transactionStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ApiTransactionStatusType {\n");
    
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("    transactionStatus: ").append(toIndentedString(transactionStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

