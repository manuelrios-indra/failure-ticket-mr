package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.telefonica.failure.types.CategoryTreeType;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.RelatedObjectType;
import com.telefonica.failure.types.RelatedPartyType;
import com.telefonica.failure.types.TicketAttachmentInfoType;
import com.telefonica.failure.types.TicketNoteInfoType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketRequestType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketRequestType   {
  @JsonProperty("correlationId")
  private String correlationId = null;

  @JsonProperty("subject")
  private String subject = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("country")
  private String country = null;

  @JsonProperty("customerId")
  private String customerId = null;

  @JsonProperty("accountId")
  private String accountId = null;

  @JsonProperty("reportedDate")
  private OffsetDateTime reportedDate = null;

  /**
   * Indication of the severity (impact) considered by the originator of the ticket
   */
  public enum SeverityEnum {
    MINOR("minor"),
    
    MODERATE("moderate"),
    
    SIGNIFICANT("significant"),
    
    EXTENSIVE("extensive"),
    
    CATASTROPHIC("catastrophic");

    private String value;

    SeverityEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static SeverityEnum fromValue(String text) {
      for (SeverityEnum b : SeverityEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("severity")
  private SeverityEnum severity = null;

  @JsonProperty("priority")
  private Integer priority = null;

  @JsonProperty("type")
  private String type = null;

  @JsonProperty("category")
  private CategoryTreeType category = null;

  @JsonProperty("source")
  private String source = null;

  @JsonProperty("parentTicket")
  private String parentTicket = null;

  @JsonProperty("relatedParty")
  @Valid
  private List<RelatedPartyType> relatedParty = null;

  @JsonProperty("relatedObject")
  @Valid
  private List<RelatedObjectType> relatedObject = null;

  @JsonProperty("note")
  @Valid
  private List<TicketNoteInfoType> note = null;

  @JsonProperty("attachment")
  @Valid
  private List<TicketAttachmentInfoType> attachment = null;

  @JsonProperty("channel")
  private ChannelRefType channel = null;

  @JsonProperty("additionalData")
  @Valid
  private List<KeyValueType> additionalData = null;

  @JsonProperty("callbackUrl")
  private String callbackUrl = null;

  @JsonProperty("statusChangeReason")
  private String statusChangeReason = null;

  /**
   * New status to be set for the ticket
   */
  public enum TicketStatusEnum {
    NEW("new"),
    
    SUBMITTED("submitted"),
    
    ACKNOWLEDGED("acknowledged"),
    
    IN_PROGRESS("in progress"),
    
    RESOLVED("resolved"),
    
    CLOSED("closed"),
    
    REOPEN("reopen"),
    
    CANCELLED("cancelled"),
    
    REJECTED("rejected"),
    
    PENDING("pending"),
    
    ASSIGNED("assigned"),
    
    HELD("held");

    private String value;

    TicketStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TicketStatusEnum fromValue(String text) {
      for (TicketStatusEnum b : TicketStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("ticketStatus")
  private TicketStatusEnum ticketStatus = null;

  public TicketRequestType correlationId(String correlationId) {
    this.correlationId = correlationId;
    return this;
  }

  /**
   * Unique identifier for the ticket created within the client, used to synchronize and map internal identifiers between server and client
   * @return correlationId
  **/
  @ApiModelProperty(value = "Unique identifier for the ticket created within the client, used to synchronize and map internal identifiers between server and client")


  public String getCorrelationId() {
    return correlationId;
  }

  public void setCorrelationId(String correlationId) {
    this.correlationId = correlationId;
  }

  public TicketRequestType subject(String subject) {
    this.subject = subject;
    return this;
  }

  /**
   * Some text providing a short description of the ticket raised
   * @return subject
  **/
  @ApiModelProperty(value = "Some text providing a short description of the ticket raised")


  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public TicketRequestType description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Some text providing a user-friendly detailed description of the ticket raised
   * @return description
  **/
  @ApiModelProperty(value = "Some text providing a user-friendly detailed description of the ticket raised")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TicketRequestType country(String country) {
    this.country = country;
    return this;
  }

  /**
   * Identifier for the country associated to the ticket
   * @return country
  **/
  @ApiModelProperty(value = "Identifier for the country associated to the ticket")


  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public TicketRequestType customerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

  /**
   * Unique Identifier for the customer originating the ticket (e.g.: OB customer number)
   * @return customerId
  **/
  @ApiModelProperty(value = "Unique Identifier for the customer originating the ticket (e.g.: OB customer number)")


  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }

  public TicketRequestType accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

  /**
   * Unique Identifier for the account within the server to be linked to the ticket (e.g.: customer account number)
   * @return accountId
  **/
  @ApiModelProperty(value = "Unique Identifier for the account within the server to be linked to the ticket (e.g.: customer account number)")


  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public TicketRequestType reportedDate(OffsetDateTime reportedDate) {
    this.reportedDate = reportedDate;
    return this;
  }

  /**
   * Date when the situation reported in the ticket was registered by the requestor (e.g.: date when an issue started to happen, date when a lack of service was detected,�). This could be required in order to manage SLA compliancy
   * @return reportedDate
  **/
  @ApiModelProperty(value = "Date when the situation reported in the ticket was registered by the requestor (e.g.: date when an issue started to happen, date when a lack of service was detected,�). This could be required in order to manage SLA compliancy")

  @Valid

  public OffsetDateTime getReportedDate() {
    return reportedDate;
  }

  public void setReportedDate(OffsetDateTime reportedDate) {
    this.reportedDate = reportedDate;
  }

  public TicketRequestType severity(SeverityEnum severity) {
    this.severity = severity;
    return this;
  }

  /**
   * Indication of the severity (impact) considered by the originator of the ticket
   * @return severity
  **/
  @ApiModelProperty(value = "Indication of the severity (impact) considered by the originator of the ticket")


  public SeverityEnum getSeverity() {
    return severity;
  }

  public void setSeverity(SeverityEnum severity) {
    this.severity = severity;
  }

  public TicketRequestType priority(Integer priority) {
    this.priority = priority;
    return this;
  }

  /**
   * Indication of the priority considered by the originator of the ticket
   * minimum: 0
   * maximum: 3
   * @return priority
  **/
  @ApiModelProperty(value = "Indication of the priority considered by the originator of the ticket")

@Min(0) @Max(3) 
  public Integer getPriority() {
    return priority;
  }

  public void setPriority(Integer priority) {
    this.priority = priority;
  }

  public TicketRequestType type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Indication of the type of ticket registered. Supported values are implementation and application specific
   * @return type
  **/
  @ApiModelProperty(value = "Indication of the type of ticket registered. Supported values are implementation and application specific")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public TicketRequestType category(CategoryTreeType category) {
    this.category = category;
    return this;
  }

  /**
   * List of categories/subcategories allocated to the ticket, intended to allow segmentation
   * @return category
  **/
  @ApiModelProperty(value = "List of categories/subcategories allocated to the ticket, intended to allow segmentation")

  @Valid

  public CategoryTreeType getCategory() {
    return category;
  }

  public void setCategory(CategoryTreeType category) {
    this.category = category;
  }

  public TicketRequestType source(String source) {
    this.source = source;
    return this;
  }

  /**
   * Indicates the origin of the ticket. Supported values are implementation and application specific
   * @return source
  **/
  @ApiModelProperty(value = "Indicates the origin of the ticket. Supported values are implementation and application specific")


  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public TicketRequestType parentTicket(String parentTicket) {
    this.parentTicket = parentTicket;
    return this;
  }

  /**
   * Unique Identifier for another ticket within the server associated to the reported one
   * @return parentTicket
  **/
  @ApiModelProperty(value = "Unique Identifier for another ticket within the server associated to the reported one")


  public String getParentTicket() {
    return parentTicket;
  }

  public void setParentTicket(String parentTicket) {
    this.parentTicket = parentTicket;
  }

  public TicketRequestType relatedParty(List<RelatedPartyType> relatedParty) {
    this.relatedParty = relatedParty;
    return this;
  }

  public TicketRequestType addRelatedPartyItem(RelatedPartyType relatedPartyItem) {
    if (this.relatedParty == null) {
      this.relatedParty = new ArrayList<RelatedPartyType>();
    }
    this.relatedParty.add(relatedPartyItem);
    return this;
  }

  /**
   * List of individuals (e.g.: originator, system impacted, reviewer, �) associated to a ticket
   * @return relatedParty
  **/
  @ApiModelProperty(value = "List of individuals (e.g.: originator, system impacted, reviewer, �) associated to a ticket")

  @Valid

  public List<RelatedPartyType> getRelatedParty() {
    return relatedParty;
  }

  public void setRelatedParty(List<RelatedPartyType> relatedParty) {
    this.relatedParty = relatedParty;
  }

  public TicketRequestType relatedObject(List<RelatedObjectType> relatedObject) {
    this.relatedObject = relatedObject;
    return this;
  }

  public TicketRequestType addRelatedObjectItem(RelatedObjectType relatedObjectItem) {
    if (this.relatedObject == null) {
      this.relatedObject = new ArrayList<RelatedObjectType>();
    }
    this.relatedObject.add(relatedObjectItem);
    return this;
  }

  /**
   * List of Objects or resources (e.g.: invoices, products, payments, �) associated to a ticket
   * @return relatedObject
  **/
  @ApiModelProperty(value = "List of Objects or resources (e.g.: invoices, products, payments, �) associated to a ticket")

  @Valid

  public List<RelatedObjectType> getRelatedObject() {
    return relatedObject;
  }

  public void setRelatedObject(List<RelatedObjectType> relatedObject) {
    this.relatedObject = relatedObject;
  }

  public TicketRequestType note(List<TicketNoteInfoType> note) {
    this.note = note;
    return this;
  }

  public TicketRequestType addNoteItem(TicketNoteInfoType noteItem) {
    if (this.note == null) {
      this.note = new ArrayList<TicketNoteInfoType>();
    }
    this.note.add(noteItem);
    return this;
  }

  /**
   * List of notes to be added as part of the creation of the ticket
   * @return note
  **/
  @ApiModelProperty(value = "List of notes to be added as part of the creation of the ticket")

  @Valid

  public List<TicketNoteInfoType> getNote() {
    return note;
  }

  public void setNote(List<TicketNoteInfoType> note) {
    this.note = note;
  }

  public TicketRequestType attachment(List<TicketAttachmentInfoType> attachment) {
    this.attachment = attachment;
    return this;
  }

  public TicketRequestType addAttachmentItem(TicketAttachmentInfoType attachmentItem) {
    if (this.attachment == null) {
      this.attachment = new ArrayList<TicketAttachmentInfoType>();
    }
    this.attachment.add(attachmentItem);
    return this;
  }

  /**
   * List of attachments to be added as part of the creation of the ticket
   * @return attachment
  **/
  @ApiModelProperty(value = "List of attachments to be added as part of the creation of the ticket")

  @Valid

  public List<TicketAttachmentInfoType> getAttachment() {
    return attachment;
  }

  public void setAttachment(List<TicketAttachmentInfoType> attachment) {
    this.attachment = attachment;
  }

  public TicketRequestType channel(ChannelRefType channel) {
    this.channel = channel;
    return this;
  }

  /**
   * Reference to the channel where the ticket was raised
   * @return channel
  **/
  @ApiModelProperty(value = "Reference to the channel where the ticket was raised")

  @Valid

  public ChannelRefType getChannel() {
    return channel;
  }

  public void setChannel(ChannelRefType channel) {
    this.channel = channel;
  }

  public TicketRequestType additionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  public TicketRequestType addAdditionalDataItem(KeyValueType additionalDataItem) {
    if (this.additionalData == null) {
      this.additionalData = new ArrayList<KeyValueType>();
    }
    this.additionalData.add(additionalDataItem);
    return this;
  }

  /**
   * Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs
   * @return additionalData
  **/
  @ApiModelProperty(value = "Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs")

  @Valid

  public List<KeyValueType> getAdditionalData() {
    return additionalData;
  }

  public void setAdditionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
  }

  public TicketRequestType callbackUrl(String callbackUrl) {
    this.callbackUrl = callbackUrl;
    return this;
  }

  /**
   * An URL that will allow the server to report asynchronously any further modification defined by the server on any of the parameters defining a ticket previously created
   * @return callbackUrl
  **/
  @ApiModelProperty(value = "An URL that will allow the server to report asynchronously any further modification defined by the server on any of the parameters defining a ticket previously created")


  public String getCallbackUrl() {
    return callbackUrl;
  }

  public void setCallbackUrl(String callbackUrl) {
    this.callbackUrl = callbackUrl;
  }

  public TicketRequestType statusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
    return this;
  }

  /**
   * Reasoning registered for the last change of status or susbstatus
   * @return statusChangeReason
  **/
  @ApiModelProperty(value = "Reasoning registered for the last change of status or susbstatus")


  public String getStatusChangeReason() {
    return statusChangeReason;
  }

  public void setStatusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
  }

  public TicketRequestType ticketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
    return this;
  }

  /**
   * New status to be set for the ticket
   * @return ticketStatus
  **/
  @ApiModelProperty(value = "New status to be set for the ticket")


  public TicketStatusEnum getTicketStatus() {
    return ticketStatus;
  }

  public void setTicketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketRequestType ticketRequestType = (TicketRequestType) o;
    return Objects.equals(this.correlationId, ticketRequestType.correlationId) &&
        Objects.equals(this.subject, ticketRequestType.subject) &&
        Objects.equals(this.description, ticketRequestType.description) &&
        Objects.equals(this.country, ticketRequestType.country) &&
        Objects.equals(this.customerId, ticketRequestType.customerId) &&
        Objects.equals(this.accountId, ticketRequestType.accountId) &&
        Objects.equals(this.reportedDate, ticketRequestType.reportedDate) &&
        Objects.equals(this.severity, ticketRequestType.severity) &&
        Objects.equals(this.priority, ticketRequestType.priority) &&
        Objects.equals(this.type, ticketRequestType.type) &&
        Objects.equals(this.category, ticketRequestType.category) &&
        Objects.equals(this.source, ticketRequestType.source) &&
        Objects.equals(this.parentTicket, ticketRequestType.parentTicket) &&
        Objects.equals(this.relatedParty, ticketRequestType.relatedParty) &&
        Objects.equals(this.relatedObject, ticketRequestType.relatedObject) &&
        Objects.equals(this.note, ticketRequestType.note) &&
        Objects.equals(this.attachment, ticketRequestType.attachment) &&
        Objects.equals(this.channel, ticketRequestType.channel) &&
        Objects.equals(this.additionalData, ticketRequestType.additionalData) &&
        Objects.equals(this.callbackUrl, ticketRequestType.callbackUrl) &&
        Objects.equals(this.statusChangeReason, ticketRequestType.statusChangeReason) &&
        Objects.equals(this.ticketStatus, ticketRequestType.ticketStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(correlationId, subject, description, country, customerId, accountId, reportedDate, severity, priority, type, category, source, parentTicket, relatedParty, relatedObject, note, attachment, channel, additionalData, callbackUrl, statusChangeReason, ticketStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketRequestType {\n");
    
    sb.append("    correlationId: ").append(toIndentedString(correlationId)).append("\n");
    sb.append("    subject: ").append(toIndentedString(subject)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    customerId: ").append(toIndentedString(customerId)).append("\n");
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    reportedDate: ").append(toIndentedString(reportedDate)).append("\n");
    sb.append("    severity: ").append(toIndentedString(severity)).append("\n");
    sb.append("    priority: ").append(toIndentedString(priority)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    category: ").append(toIndentedString(category)).append("\n");
    sb.append("    source: ").append(toIndentedString(source)).append("\n");
    sb.append("    parentTicket: ").append(toIndentedString(parentTicket)).append("\n");
    sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
    sb.append("    relatedObject: ").append(toIndentedString(relatedObject)).append("\n");
    sb.append("    note: ").append(toIndentedString(note)).append("\n");
    sb.append("    attachment: ").append(toIndentedString(attachment)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("    callbackUrl: ").append(toIndentedString(callbackUrl)).append("\n");
    sb.append("    statusChangeReason: ").append(toIndentedString(statusChangeReason)).append("\n");
    sb.append("    ticketStatus: ").append(toIndentedString(ticketStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

