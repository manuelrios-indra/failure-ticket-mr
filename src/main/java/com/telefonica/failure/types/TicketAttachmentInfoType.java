package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketAttachmentInfoType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketAttachmentInfoType   {
  @JsonProperty("attachmentId")
  private String attachmentId = null;

  @JsonProperty("creationDate")
  private OffsetDateTime creationDate = null;

  @JsonProperty("author")
  private String author = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("documentLink")
  private String documentLink = null;

  @JsonProperty("channel")
  private ChannelRefType channel = null;

  @JsonProperty("additionalData")
  @Valid
  private List<KeyValueType> additionalData = null;

  public TicketAttachmentInfoType attachmentId(String attachmentId) {
    this.attachmentId = attachmentId;
    return this;
  }

  /**
   * Unique Identifier among the attachments linked to a ticket within the server
   * @return attachmentId
  **/
  @ApiModelProperty(required = true, value = "Unique Identifier among the attachments linked to a ticket within the server")
  @NotNull


  public String getAttachmentId() {
    return attachmentId;
  }

  public void setAttachmentId(String attachmentId) {
    this.attachmentId = attachmentId;
  }

  public TicketAttachmentInfoType creationDate(OffsetDateTime creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  /**
   * Date when the attachment was associated to the ticket
   * @return creationDate
  **/
  @ApiModelProperty(required = true, value = "Date when the attachment was associated to the ticket")
  @NotNull

  @Valid

  public OffsetDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(OffsetDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public TicketAttachmentInfoType author(String author) {
    this.author = author;
    return this;
  }

  /**
   * Identification of the originator of the attachment. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal
   * @return author
  **/
  @ApiModelProperty(value = "Identification of the originator of the attachment. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal")


  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public TicketAttachmentInfoType name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Some text providing a short description of the attachment
   * @return name
  **/
  @ApiModelProperty(required = true, value = "Some text providing a short description of the attachment")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TicketAttachmentInfoType documentLink(String documentLink) {
    this.documentLink = documentLink;
    return this;
  }

  /**
   * A resource URI pointing to the resource in the server that stores the attachment details (e.g.: an image)
   * @return documentLink
  **/
  @ApiModelProperty(value = "A resource URI pointing to the resource in the server that stores the attachment details (e.g.: an image)")


  public String getDocumentLink() {
    return documentLink;
  }

  public void setDocumentLink(String documentLink) {
    this.documentLink = documentLink;
  }

  public TicketAttachmentInfoType channel(ChannelRefType channel) {
    this.channel = channel;
    return this;
  }

  /**
   * channel where the attachment was uploaded/created
   * @return channel
  **/
  @ApiModelProperty(value = "channel where the attachment was uploaded/created")

  @Valid

  public ChannelRefType getChannel() {
    return channel;
  }

  public void setChannel(ChannelRefType channel) {
    this.channel = channel;
  }

  public TicketAttachmentInfoType additionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  public TicketAttachmentInfoType addAdditionalDataItem(KeyValueType additionalDataItem) {
    if (this.additionalData == null) {
      this.additionalData = new ArrayList<KeyValueType>();
    }
    this.additionalData.add(additionalDataItem);
    return this;
  }

  /**
   * Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs
   * @return additionalData
  **/
  @ApiModelProperty(value = "Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs")

  @Valid

  public List<KeyValueType> getAdditionalData() {
    return additionalData;
  }

  public void setAdditionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketAttachmentInfoType ticketAttachmentInfoType = (TicketAttachmentInfoType) o;
    return Objects.equals(this.attachmentId, ticketAttachmentInfoType.attachmentId) &&
        Objects.equals(this.creationDate, ticketAttachmentInfoType.creationDate) &&
        Objects.equals(this.author, ticketAttachmentInfoType.author) &&
        Objects.equals(this.name, ticketAttachmentInfoType.name) &&
        Objects.equals(this.documentLink, ticketAttachmentInfoType.documentLink) &&
        Objects.equals(this.channel, ticketAttachmentInfoType.channel) &&
        Objects.equals(this.additionalData, ticketAttachmentInfoType.additionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attachmentId, creationDate, author, name, documentLink, channel, additionalData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketAttachmentInfoType {\n");
    
    sb.append("    attachmentId: ").append(toIndentedString(attachmentId)).append("\n");
    sb.append("    creationDate: ").append(toIndentedString(creationDate)).append("\n");
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    documentLink: ").append(toIndentedString(documentLink)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

