package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.TicketStatusHistoryItemType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketStatusInfoType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketStatusInfoType   {
  @JsonProperty("statusChangeDate")
  private OffsetDateTime statusChangeDate = null;

  @JsonProperty("statusChangeReason")
  private String statusChangeReason = null;

  @JsonProperty("statusChangeChannel")
  private ChannelRefType statusChangeChannel = null;

  @JsonProperty("ticketId")
  private String ticketId = null;

  /**
   * Status of the ticket
   */
  public enum TicketStatusEnum {
    NEW("new"),
    
    SUBMITTED("submitted"),
    
    ACKNOWLEDGED("acknowledged"),
    
    IN_PROGRESS("in progress"),
    
    RESOLVED("resolved"),
    
    CLOSED("closed"),
    
    REOPEN("reopen"),
    
    CANCELLED("cancelled"),
    
    REJECTED("rejected"),
    
    PENDING("pending"),
    
    ASSIGNED("assigned"),
    
    HELD("held");

    private String value;

    TicketStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TicketStatusEnum fromValue(String text) {
      for (TicketStatusEnum b : TicketStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("ticketStatus")
  private TicketStatusEnum ticketStatus = null;

  @JsonProperty("ticketSubstatus")
  private String ticketSubstatus = null;

  @JsonProperty("clientData")
  @Valid
  private List<KeyValueType> clientData = null;

  @JsonProperty("statusHistory")
  @Valid
  private List<TicketStatusHistoryItemType> statusHistory = null;

  public TicketStatusInfoType statusChangeDate(OffsetDateTime statusChangeDate) {
    this.statusChangeDate = statusChangeDate;
    return this;
  }

  /**
   * Date registered for the last change of status or substatus
   * @return statusChangeDate
  **/
  @ApiModelProperty(required = true, value = "Date registered for the last change of status or substatus")
  @NotNull

  @Valid

  public OffsetDateTime getStatusChangeDate() {
    return statusChangeDate;
  }

  public void setStatusChangeDate(OffsetDateTime statusChangeDate) {
    this.statusChangeDate = statusChangeDate;
  }

  public TicketStatusInfoType statusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
    return this;
  }

  /**
   * Reasoning registered for the last change of status or susbstatus
   * @return statusChangeReason
  **/
  @ApiModelProperty(value = "Reasoning registered for the last change of status or susbstatus")


  public String getStatusChangeReason() {
    return statusChangeReason;
  }

  public void setStatusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
  }

  public TicketStatusInfoType statusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
    return this;
  }

  /**
   * channel where the status was last changed
   * @return statusChangeChannel
  **/
  @ApiModelProperty(value = "channel where the status was last changed")

  @Valid

  public ChannelRefType getStatusChangeChannel() {
    return statusChangeChannel;
  }

  public void setStatusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
  }

  public TicketStatusInfoType ticketId(String ticketId) {
    this.ticketId = ticketId;
    return this;
  }

  /**
   * Unique Identifier within the server for the ticket reported
   * @return ticketId
  **/
  @ApiModelProperty(required = true, value = "Unique Identifier within the server for the ticket reported")
  @NotNull


  public String getTicketId() {
    return ticketId;
  }

  public void setTicketId(String ticketId) {
    this.ticketId = ticketId;
  }

  public TicketStatusInfoType ticketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
    return this;
  }

  /**
   * Status of the ticket
   * @return ticketStatus
  **/
  @ApiModelProperty(required = true, value = "Status of the ticket")
  @NotNull


  public TicketStatusEnum getTicketStatus() {
    return ticketStatus;
  }

  public void setTicketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
  }

  public TicketStatusInfoType ticketSubstatus(String ticketSubstatus) {
    this.ticketSubstatus = ticketSubstatus;
    return this;
  }

  /**
   * Substatus in order to define a second status level
   * @return ticketSubstatus
  **/
  @ApiModelProperty(value = "Substatus in order to define a second status level")


  public String getTicketSubstatus() {
    return ticketSubstatus;
  }

  public void setTicketSubstatus(String ticketSubstatus) {
    this.ticketSubstatus = ticketSubstatus;
  }

  public TicketStatusInfoType clientData(List<KeyValueType> clientData) {
    this.clientData = clientData;
    return this;
  }

  public TicketStatusInfoType addClientDataItem(KeyValueType clientDataItem) {
    if (this.clientData == null) {
      this.clientData = new ArrayList<KeyValueType>();
    }
    this.clientData.add(clientDataItem);
    return this;
  }

  /**
   * Any further information related to client needed by the server to fill the entity definition.
   * @return clientData
  **/
  @ApiModelProperty(value = "Any further information related to client needed by the server to fill the entity definition.")

  @Valid

  public List<KeyValueType> getClientData() {
    return clientData;
  }

  public void setClientData(List<KeyValueType> clientData) {
    this.clientData = clientData;
  }

  public TicketStatusInfoType statusHistory(List<TicketStatusHistoryItemType> statusHistory) {
    this.statusHistory = statusHistory;
    return this;
  }

  public TicketStatusInfoType addStatusHistoryItem(TicketStatusHistoryItemType statusHistoryItem) {
    if (this.statusHistory == null) {
      this.statusHistory = new ArrayList<TicketStatusHistoryItemType>();
    }
    this.statusHistory.add(statusHistoryItem);
    return this;
  }

  /**
   * Get statusHistory
   * @return statusHistory
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<TicketStatusHistoryItemType> getStatusHistory() {
    return statusHistory;
  }

  public void setStatusHistory(List<TicketStatusHistoryItemType> statusHistory) {
    this.statusHistory = statusHistory;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketStatusInfoType ticketStatusInfoType = (TicketStatusInfoType) o;
    return Objects.equals(this.statusChangeDate, ticketStatusInfoType.statusChangeDate) &&
        Objects.equals(this.statusChangeReason, ticketStatusInfoType.statusChangeReason) &&
        Objects.equals(this.statusChangeChannel, ticketStatusInfoType.statusChangeChannel) &&
        Objects.equals(this.ticketId, ticketStatusInfoType.ticketId) &&
        Objects.equals(this.ticketStatus, ticketStatusInfoType.ticketStatus) &&
        Objects.equals(this.ticketSubstatus, ticketStatusInfoType.ticketSubstatus) &&
        Objects.equals(this.clientData, ticketStatusInfoType.clientData) &&
        Objects.equals(this.statusHistory, ticketStatusInfoType.statusHistory);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusChangeDate, statusChangeReason, statusChangeChannel, ticketId, ticketStatus, ticketSubstatus, clientData, statusHistory);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketStatusInfoType {\n");
    
    sb.append("    statusChangeDate: ").append(toIndentedString(statusChangeDate)).append("\n");
    sb.append("    statusChangeReason: ").append(toIndentedString(statusChangeReason)).append("\n");
    sb.append("    statusChangeChannel: ").append(toIndentedString(statusChangeChannel)).append("\n");
    sb.append("    ticketId: ").append(toIndentedString(ticketId)).append("\n");
    sb.append("    ticketStatus: ").append(toIndentedString(ticketStatus)).append("\n");
    sb.append("    ticketSubstatus: ").append(toIndentedString(ticketSubstatus)).append("\n");
    sb.append("    clientData: ").append(toIndentedString(clientData)).append("\n");
    sb.append("    statusHistory: ").append(toIndentedString(statusHistory)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

