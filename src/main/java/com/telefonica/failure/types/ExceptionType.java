package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * ExceptionType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class ExceptionType   {
  @JsonProperty("exceptionId")
  private String exceptionId = null;

  @JsonProperty("exceptionText")
  private String exceptionText = null;

  @JsonProperty("moreInfo")
  private String moreInfo = null;

  @JsonProperty("userMessage")
  private String userMessage = null;

  public ExceptionType exceptionId(String exceptionId) {
    this.exceptionId = exceptionId;
    return this;
  }

  /**
   * Identifier of the exception
   * @return exceptionId
  **/
  @ApiModelProperty(required = true, value = "Identifier of the exception")
  @NotNull


  public String getExceptionId() {
    return exceptionId;
  }

  public void setExceptionId(String exceptionId) {
    this.exceptionId = exceptionId;
  }

  public ExceptionType exceptionText(String exceptionText) {
    this.exceptionText = exceptionText;
    return this;
  }

  /**
   * Human readable description of the associated error, including some specific variables
   * @return exceptionText
  **/
  @ApiModelProperty(required = true, value = "Human readable description of the associated error, including some specific variables")
  @NotNull


  public String getExceptionText() {
    return exceptionText;
  }

  public void setExceptionText(String exceptionText) {
    this.exceptionText = exceptionText;
  }

  public ExceptionType moreInfo(String moreInfo) {
    this.moreInfo = moreInfo;
    return this;
  }

  /**
   * A URI where more information about the exception is provided
   * @return moreInfo
  **/
  @ApiModelProperty(value = "A URI where more information about the exception is provided")


  public String getMoreInfo() {
    return moreInfo;
  }

  public void setMoreInfo(String moreInfo) {
    this.moreInfo = moreInfo;
  }

  public ExceptionType userMessage(String userMessage) {
    this.userMessage = userMessage;
    return this;
  }

  /**
   * A message that can be shown to the user of the Application implementing the API Client
   * @return userMessage
  **/
  @ApiModelProperty(value = "A message that can be shown to the user of the Application implementing the API Client")


  public String getUserMessage() {
    return userMessage;
  }

  public void setUserMessage(String userMessage) {
    this.userMessage = userMessage;
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ExceptionType exceptionType = (ExceptionType) o;
    return Objects.equals(this.exceptionId, exceptionType.exceptionId) &&
        Objects.equals(this.exceptionText, exceptionType.exceptionText) &&
        Objects.equals(this.moreInfo, exceptionType.moreInfo) &&
        Objects.equals(this.userMessage, exceptionType.userMessage);
  }

  @Override
  public int hashCode() {
    return Objects.hash(exceptionId, exceptionText, moreInfo, userMessage);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ExceptionType {\n");
    
    sb.append("    exceptionId: ").append(toIndentedString(exceptionId)).append("\n");
    sb.append("    exceptionText: ").append(toIndentedString(exceptionText)).append("\n");
    sb.append("    moreInfo: ").append(toIndentedString(moreInfo)).append("\n");
    sb.append("    userMessage: ").append(toIndentedString(userMessage)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

