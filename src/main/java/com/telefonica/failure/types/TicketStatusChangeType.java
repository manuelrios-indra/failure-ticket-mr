package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketStatusChangeType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketStatusChangeType   {
  @JsonProperty("statusChangeReason")
  private String statusChangeReason = null;

  /**
   * New status to be set for the ticket
   */
  public enum TicketStatusEnum {
    NEW("new"),
    
    SUBMITTED("submitted"),
    
    ACKNOWLEDGED("acknowledged"),
    
    IN_PROGRESS("in progress"),
    
    RESOLVED("resolved"),
    
    CLOSED("closed"),
    
    REOPEN("reopen"),
    
    CANCELLED("cancelled"),
    
    REJECTED("rejected"),
    
    PENDING("pending"),
    
    ASSIGNED("assigned"),
    
    HELD("held");

    private String value;

    TicketStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TicketStatusEnum fromValue(String text) {
      for (TicketStatusEnum b : TicketStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("ticketStatus")
  private TicketStatusEnum ticketStatus = null;

  @JsonProperty("ticketSubstatus")
  private String ticketSubstatus = null;

  @JsonProperty("statusChangeChannel")
  private ChannelRefType statusChangeChannel = null;

  @JsonProperty("clientData")
  @Valid
  private List<KeyValueType> clientData = null;

  public TicketStatusChangeType statusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
    return this;
  }

  /**
   * Reasoning registered for the last change of status or susbstatus
   * @return statusChangeReason
  **/
  @ApiModelProperty(value = "Reasoning registered for the last change of status or susbstatus")


  public String getStatusChangeReason() {
    return statusChangeReason;
  }

  public void setStatusChangeReason(String statusChangeReason) {
    this.statusChangeReason = statusChangeReason;
  }

  public TicketStatusChangeType ticketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
    return this;
  }

  /**
   * New status to be set for the ticket
   * @return ticketStatus
  **/
  @ApiModelProperty(required = true, value = "New status to be set for the ticket")
  @NotNull


  public TicketStatusEnum getTicketStatus() {
    return ticketStatus;
  }

  public void setTicketStatus(TicketStatusEnum ticketStatus) {
    this.ticketStatus = ticketStatus;
  }

  public TicketStatusChangeType ticketSubstatus(String ticketSubstatus) {
    this.ticketSubstatus = ticketSubstatus;
    return this;
  }

  /**
   * Substatus in order to define a second status level
   * @return ticketSubstatus
  **/
  @ApiModelProperty(value = "Substatus in order to define a second status level")


  public String getTicketSubstatus() {
    return ticketSubstatus;
  }

  public void setTicketSubstatus(String ticketSubstatus) {
    this.ticketSubstatus = ticketSubstatus;
  }

  public TicketStatusChangeType statusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
    return this;
  }

  /**
   * channel where the status was last changed
   * @return statusChangeChannel
  **/
  @ApiModelProperty(value = "channel where the status was last changed")

  @Valid

  public ChannelRefType getStatusChangeChannel() {
    return statusChangeChannel;
  }

  public void setStatusChangeChannel(ChannelRefType statusChangeChannel) {
    this.statusChangeChannel = statusChangeChannel;
  }

  public TicketStatusChangeType clientData(List<KeyValueType> clientData) {
    this.clientData = clientData;
    return this;
  }

  public TicketStatusChangeType addClientDataItem(KeyValueType clientDataItem) {
    if (this.clientData == null) {
      this.clientData = new ArrayList<KeyValueType>();
    }
    this.clientData.add(clientDataItem);
    return this;
  }

  /**
   * Any further information related to client needed by the server to fill the entity definition.
   * @return clientData
  **/
  @ApiModelProperty(value = "Any further information related to client needed by the server to fill the entity definition.")

  @Valid

  public List<KeyValueType> getClientData() {
    return clientData;
  }

  public void setClientData(List<KeyValueType> clientData) {
    this.clientData = clientData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketStatusChangeType ticketStatusChangeType = (TicketStatusChangeType) o;
    return Objects.equals(this.statusChangeReason, ticketStatusChangeType.statusChangeReason) &&
        Objects.equals(this.ticketStatus, ticketStatusChangeType.ticketStatus) &&
        Objects.equals(this.ticketSubstatus, ticketStatusChangeType.ticketSubstatus) &&
        Objects.equals(this.statusChangeChannel, ticketStatusChangeType.statusChangeChannel) &&
        Objects.equals(this.clientData, ticketStatusChangeType.clientData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(statusChangeReason, ticketStatus, ticketSubstatus, statusChangeChannel, clientData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketStatusChangeType {\n");
    
    sb.append("    statusChangeReason: ").append(toIndentedString(statusChangeReason)).append("\n");
    sb.append("    ticketStatus: ").append(toIndentedString(ticketStatus)).append("\n");
    sb.append("    ticketSubstatus: ").append(toIndentedString(ticketSubstatus)).append("\n");
    sb.append("    statusChangeChannel: ").append(toIndentedString(statusChangeChannel)).append("\n");
    sb.append("    clientData: ").append(toIndentedString(clientData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

