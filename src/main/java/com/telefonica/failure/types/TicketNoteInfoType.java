package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.TicketAttachmentType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.OffsetDateTime;

/**
 * TicketNoteInfoType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketNoteInfoType {

    @JsonProperty("date")
    private OffsetDateTime date = null;

    @JsonProperty("author")
    private String author = null;

    @JsonProperty("text")
    private String text = null;

    public TicketNoteInfoType date(OffsetDateTime date) {
	this.date = date;
	return this;
    }

    /**
     * Contents of the note (comment) to be associated to the ticket
     * 
     * @return text
     **/
    @ApiModelProperty(required = true, value = "Date of the registered failure or claim")
    @NotNull

    public OffsetDateTime getDate() {
	return date;
    }

    public void setDate(OffsetDateTime date) {
	this.date = date;
    }

    public TicketNoteInfoType author(String author) {
	this.author = author;
	return this;
    }

    /**
     * Identification of the originator of the note. Meaning of this is
     * implementation and application specific, it could be, for instance, the login
     * name of a persona that could access to read/modify the ticket details via a
     * web portal
     * 
     * @return author
     **/
    @ApiModelProperty(required = true, value = "Identification of the originator of the note. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal")
    @NotNull

    public String getAuthor() {
	return author;
    }

    public void setAuthor(String author) {
	this.author = author;
    }

    public TicketNoteInfoType text(String text) {
	this.text = text;
	return this;
    }

    /**
     * Contents of the note (comment) to be associated to the ticket
     * 
     * @return text
     **/
    @ApiModelProperty(required = true, value = "Contents of the note (comment) to be associated to the ticket")
    @NotNull

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((author == null) ? 0 : author.hashCode());
	result = prime * result + ((date == null) ? 0 : date.hashCode());
	result = prime * result + ((text == null) ? 0 : text.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	TicketNoteInfoType other = (TicketNoteInfoType) obj;
	if (author == null) {
	    if (other.author != null)
		return false;
	} else if (!author.equals(other.author))
	    return false;
	if (date == null) {
	    if (other.date != null)
		return false;
	} else if (!date.equals(other.date))
	    return false;
	if (text == null) {
	    if (other.text != null)
		return false;
	} else if (!text.equals(other.text))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "TicketNoteInfoType [date=" + date + ", author=" + author + ", text=" + text + "]";
    }

}