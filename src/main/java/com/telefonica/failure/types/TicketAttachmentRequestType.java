package com.telefonica.failure.types;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.telefonica.failure.types.ChannelRefType;
import com.telefonica.failure.types.KeyValueType;
import com.telefonica.failure.types.TicketAttachmentType;

import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * TicketAttachmentRequestType
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-03-27T15:30:10.935Z")

public class TicketAttachmentRequestType   {
  @JsonProperty("attachment")
  private TicketAttachmentType attachment = null;

  @JsonProperty("author")
  private String author = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("documentLink")
  private String documentLink = null;

  @JsonProperty("channel")
  private ChannelRefType channel = null;

  @JsonProperty("additionalData")
  @Valid
  private List<KeyValueType> additionalData = null;

  public TicketAttachmentRequestType attachment(TicketAttachmentType attachment) {
    this.attachment = attachment;
    return this;
  }

  /**
   * Structure including the contents of the attachment provided by the client to be stored by the server
   * @return attachment
  **/
  @ApiModelProperty(required = true, value = "Structure including the contents of the attachment provided by the client to be stored by the server")
  @NotNull

  @Valid

  public TicketAttachmentType getAttachment() {
    return attachment;
  }

  public void setAttachment(TicketAttachmentType attachment) {
    this.attachment = attachment;
  }

  public TicketAttachmentRequestType author(String author) {
    this.author = author;
    return this;
  }

  /**
   * Identification of the originator of the attachment. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal
   * @return author
  **/
  @ApiModelProperty(value = "Identification of the originator of the attachment. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the ticket details via a web portal")


  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public TicketAttachmentRequestType name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Some text providing a short description of the attachment
   * @return name
  **/
  @ApiModelProperty(required = true, value = "Some text providing a short description of the attachment")
  @NotNull


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TicketAttachmentRequestType documentLink(String documentLink) {
    this.documentLink = documentLink;
    return this;
  }

  /**
   * An URI pointing to an addressable resource that stores the attachment details (e.g.: an image)
   * @return documentLink
  **/
  @ApiModelProperty(value = "An URI pointing to an addressable resource that stores the attachment details (e.g.: an image)")


  public String getDocumentLink() {
    return documentLink;
  }

  public void setDocumentLink(String documentLink) {
    this.documentLink = documentLink;
  }

  public TicketAttachmentRequestType channel(ChannelRefType channel) {
    this.channel = channel;
    return this;
  }

  /**
   * channel where the attachment was uploaded/created
   * @return channel
  **/
  @ApiModelProperty(value = "channel where the attachment was uploaded/created")

  @Valid

  public ChannelRefType getChannel() {
    return channel;
  }

  public void setChannel(ChannelRefType channel) {
    this.channel = channel;
  }

  public TicketAttachmentRequestType additionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
    return this;
  }

  public TicketAttachmentRequestType addAdditionalDataItem(KeyValueType additionalDataItem) {
    if (this.additionalData == null) {
      this.additionalData = new ArrayList<KeyValueType>();
    }
    this.additionalData.add(additionalDataItem);
    return this;
  }

  /**
   * Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs
   * @return additionalData
  **/
  @ApiModelProperty(value = "Any further information needed by the server to fill the attachment definition. It is recommended not to use this parameter since next releases of the UNICA API will not include its support because it has been detected that the extensibility function is not helping the stability of the standard definition of UNICA APIs")

  @Valid

  public List<KeyValueType> getAdditionalData() {
    return additionalData;
  }

  public void setAdditionalData(List<KeyValueType> additionalData) {
    this.additionalData = additionalData;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TicketAttachmentRequestType ticketAttachmentRequestType = (TicketAttachmentRequestType) o;
    return Objects.equals(this.attachment, ticketAttachmentRequestType.attachment) &&
        Objects.equals(this.author, ticketAttachmentRequestType.author) &&
        Objects.equals(this.name, ticketAttachmentRequestType.name) &&
        Objects.equals(this.documentLink, ticketAttachmentRequestType.documentLink) &&
        Objects.equals(this.channel, ticketAttachmentRequestType.channel) &&
        Objects.equals(this.additionalData, ticketAttachmentRequestType.additionalData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(attachment, author, name, documentLink, channel, additionalData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TicketAttachmentRequestType {\n");
    
    sb.append("    attachment: ").append(toIndentedString(attachment)).append("\n");
    sb.append("    author: ").append(toIndentedString(author)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    documentLink: ").append(toIndentedString(documentLink)).append("\n");
    sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
    sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

