package com.telefonica.failure.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * 
 * @Author:
 * @Datecreation: apr. 2020
 * @FileName: CMS.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Clase generada para la invocación de la tabla AVERIAS_CMS
 */
@Data
@Entity
@Table(name = "AVERIAS_CMS", schema = "P_DAAS_DATA")
public class CMS {
	
	@Column(name = "NUM_TEL")
	private String telephoneNumber;
	
	@Column(name = "COD_CLIENTE")
	private String clientCode;

	@Column(name = "COD_CUENTA")
	private String accountCode;
	
	@Column(name = "NUM_INSCRIPCION")
	private String inscriptionNumber;
	
	@Column(name = "COD_SERV_TV")
	private String tvServiceCode;
	
	@Id
	@Column(name = "ID_AVERIA_CMS")
	private String failureCMSId;
	
	@Column(name = "FECHA_HORA_AVERIA")
	private Date failureDate;
	
	@Column(name = "USU_REG_AVERIA")
	private String failureRegisteredUser;
	
	@Column(name = "AREA_REG_AVERIA")
	private String failureRegisteredChannel;
	
	@Column(name = "COD_MOTIVO_AVERIA")
	private String failureReasonCode;
	
	@Column(name = "DESC_MOTIVO_AVERIA")
	private String failureReasonDescription;

	@Column(name = "COD_ESTADO_AVERIA")
	private String failureStatusCode;
	
	@Column(name = "DESC_ESTADO_AVERIA")
	private String failureStatusDescription;
	
	@Column(name = "OBS_AVERIA")
	private String failureRemark;
	
	@Column(name = "FECHA_HORA_ACT_AVERIA")
	private Date failureLastChangeDate;
	
	@Column(name = "USU_ACT_AVERIA")
	private String failureLastChangeUser;
	
	@Column(name = "AREA_ACT_AVERIA")
	private String failureLastChangeUserChannel;
	
	@Column(name = "COD_LIQUI")
	private String resolutionCode;
	
	@Column(name = "DESC_LIQUI")
	private String resolutionDescription;
	
	@Column(name = "COD_DETA_LIQUI")
	private String resolutionReasonCode;
	
	@Column(name = "DESC_DETA_LIQUI")
	private String resolutionReasonDescription;
	
	@Column(name = "FECHA_HORA_LIQUI")
	private Date resolutionDate;
	
	@Column(name = "USU_LIQUI")
	private String resolutionUser;
	
	@Column(name = "AREA_LIQUI")
	private String resolutionUserChannel;
	
	@Column(name = "OBS_LIQUI")
	private String resolutionUserRemark;	
	
	@Column(name = "FECHA_HORA_EXTRACCION")
	private Date extractionDate;

}
