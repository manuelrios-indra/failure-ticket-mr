package com.telefonica.failure.commons;

public class Ticket {

    public static final String HREF = "/ticket/v2/tickets/";

    /**
     * Valores estaticos para type en el request
     */
    public final static String BREAKDOWN = "Avería";
    public final static String CLAIM	 = "Reclamo";

    /**
     * Valores estaticos para type en el response
     */
    public final static String ALDM   = "Amdocs";
    public final static String GESTEL = "Gestel";
    public final static String CMS    = "CMS";
    public final static String FENIX  = "Fenix";

    /**
     * Valores estaticos para relatedObject
     */
    public final static String MOBILE	 = "mobile";
    public final static String LANDLINE	 = "landline";
    public final static String IPTV	 = "ipTv";
    public final static String CABLETV	 = "cableTv";
    public final static String EMAIL	 = "email";
    public final static String BROADBAND = "broadband";
    public final static String PRODUCT	 = "product";
    public final static String BILL	 = "bill";

    /**
     * Valores estaticos para relatedParty
     */
    public final static String CREATIONUSER    = "creationUser";
    public final static String SATUSCHANGEUSER = "statusChangeUser";
    public final static String RESOLUTIONUSER  = "resolutionUser";

    /**
     * Valores estaticos para additionalData
     */
    public final static String INVOICEISSUEDATE	    = "Fecha de Emisión de Factura";
    public final static String INSCRIPTION	    = "Inscripción";
    public final static String CLAIMNUMBER	    = "Número de Reclamo";
    public final static String REPORTNUMBER	    = "Número de Reporte";
    public final static String INDICATOR	    = "Indicador";
    public final static String DATEMASSBREAKDOWN    = "Fecha y Hora de Avería Masiva";
    public final static String SCHEDULEDWORKCODE    = "Código de Trabajo Programado";
    public final static String SCHEDULEDWORKDATE    = "Fecha de Trabajo Programado";
    public final static String SCHEDULEDWORKENDTIME = "Hora Fin de Trabajo Programado";

    /**
     * Formato de Fecha
     */
    public static final String DATE_TIME = "dd/MM/yy ' ' HH:mm:ss";
    public static final String TIME	 = "HH:mm:ss";

    /**
     * Gestión de Log y entornos.
     */
    public static final String NEW_LINE		      = System.getProperty("line.separator");
    public static final String CLASS_LOG_LABEL	      = "[Class]: ";
    public static final String METHOD_LOG_LABEL	      = "[Method]: ";
    public static final String PARAMETERS_LOG_LABEL   = "(...)";
    public static final String INPUT_PARAMETERS_LABEL = "[Input Params]: ";
    public static final String INPUT_PARAMETER_LABEL  = "[Input]: ";
    public static final String OUTPUT_LABEL	      = "[Output]: ";
    public static final String SEPARATOR	      = "===================================================================================================================================================================================";
    public static final String EXCEPTION_WAS_THROWN   = "-> An exception was thrown by method: ";
    public static final String PARSE_JSON_RESPONSE    = "[No se pudo analizar el JSON del response del método (Verificar el Log)]";
    public static final String DEV_ENVIRONMENT	      = "dev";

    /**
     * codigos de error
     */
    public static final String GENERIC_CODE   = "0000";
    public static final String CODE_0001      = "0001";
    public static final String CODE_0002      = "0002";
    public static final String TICKETNOTFOUND = "Any tickect found related to: ";
    public static final String INCORRECTROI   = "Incorrect parameter relatedObjectInvolvement: ";
    public static final String INCORRECTTYPE  = "Incorrect parameter type: ";

    /**
     * Constantes auxiliares
     */
    public static final String VERTICALLINE = "|";
    public static final String HYPHEN	    = "-";
    public static final String EMPTY	    = "";

    /**
     * Exception mostrados en Swagger
     */
    public static final String SVC0001_CODE	   = "SVC0001";
    public static final String SVC0001_EXCEPTION   = "Generic Client Error";
    public static final String SVC0001_DESCRIPTION = "API Generic wildcard fault response.";
    public static final String SVC0001_TEXT	   = "Generic Client Error: ";

    public static final String SVC1000_CODE	   = "SVC1000";
    public static final String SVC1000_EXCEPTION   = "Missing mandatory parameter";
    public static final String SVC1000_DESCRIPTION = "API Request withour mandatory field";
    public static final String SVC1000_TEXT	   = "Missing mandatory parameter: ";

    public static final String SVC1001_CODE	   = "SVC1001";
    public static final String SVC1001_EXCEPTION   = "Invalid parameter";
    public static final String SVC1001_DESCRIPTION = "API Request with an element not conforming to swagger definitions or to a list a allowed Query Parameters";
    public static final String SVC1001_TEXT	   = "Invalid parameter: ";

    public static final String SVC1003_CODE	   = "SVC1003";
    public static final String SVC1003_EXCEPTION   = "Invalid Requested Operation";
    public static final String SVC1003_DESCRIPTION = "Requested Operation does not exist.";
    public static final String SVC1003_TEXT	   = "Requested HTTP Method Operation does not exist.";

    public static final String SVC1006_CODE	   = "SVC1006";
    public static final String SVC1006_EXCEPTION   = "Not existing Resource Id";
    public static final String SVC1006_DESCRIPTION = "Reference to a resource identifier which does not exist in the collection/repository referred (e.g.: invalid Id)";
    public static final String SVC1006_TEXT	   = "Resource does not exist";

    public static final String SVR1000_CODE	   = "SVR1000";
    public static final String SVR1000_EXCEPTION   = "Generic Server Fault";
    public static final String SVR1000_DESCRIPTION = "There was a problem in the Service Providers network that prevented to carry out the request.";
    public static final String SVR1000_TEXT	   = "Generic Server Error: ";

    public static final String SVR1008_CODE	   = "SVR1008";
    public static final String SVR1008_EXCEPTION   = "Timeout processing request";
    public static final String SVR1008_DESCRIPTION = "There was a timeout in the server side while processing the request.";
    public static final String SVR1008_TEXT	   = "Timeout processing request: ";

}