package com.telefonica.failure.commons;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 * 
 * @Author: Alexandra Huamani Condor.
 * @Datecreation: 22 ago. 2019 11:42:45
 * @FileName: UtilLog.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 */
public class Util {

    private static OffsetDateTime offsetdatetime;

    /**
     * Método de formato de fecha y hora utilizado en el response (Json).
     * 
     * @return OffsetDateTime
     */
    public static OffsetDateTime getOffsetDateTime(Date date) {

	if (date != null) {
	    Instant instantFromDate = date.toInstant();
	    ZoneOffset zoneOffSet = ZoneOffset.of("-05:00");
	    offsetdatetime = instantFromDate.atOffset(zoneOffSet);
	} else {
	    offsetdatetime = null;
	}
	return offsetdatetime;
    }

    public static Date threeMonthsAgo() {
	Date referenceDate = new Date();
	Calendar c = Calendar.getInstance();
	c.setTime(referenceDate);
	c.add(Calendar.MONTH, -3);
	return c.getTime();
    }

    public static void generateTracking() {
	Thread.currentThread().setName(UUID.randomUUID().toString().replace("-", ""));
    }

    public static String getTracking() {
	return Thread.currentThread().getName();
    }

    /**
     * Método de formato de fecha y hora utilizado en la clase LoggingAspect.
     * 
     * @return date
     */
    public static String getDateTimeFormatter() {
	DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Ticket.DATE_TIME);
	LocalDateTime localDateTime = LocalDateTime.now();
	String date = dateTimeFormatter.format(localDateTime);
	return date;
    }

    public static String getDateFormat(Date date, String exp) {
	SimpleDateFormat sdf = new SimpleDateFormat(exp);
	return sdf.format(date);
    }

}
