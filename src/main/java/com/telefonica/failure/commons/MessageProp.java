package com.telefonica.failure.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource(value = { "classpath:message.properties" })
public class MessageProp {

    @Autowired
    private Environment environment;

    public String getMessageProp(String key) {
	if (key == null) {
	    return environment.getProperty(Ticket.GENERIC_CODE);
	}
	if (environment.containsProperty(key)) {
	    return environment.getProperty(key);
	} else {
	    return environment.getProperty(Ticket.GENERIC_CODE);
	}
    }
}
