package com.telefonica.failure.commons;

import com.telefonica.failure.types.TicketDetailType.StatusEnum;

public class StatusFactory {

    public static String fromALDM(String status) {

	StatusEnum statusEnum;

	switch (status) {
	case "Abierto":
	    statusEnum = StatusEnum.NEW;
	    break;
	case "Aplicación de tratamiento":
	    statusEnum = StatusEnum.RESOLVED;
	    break;
	case "Cerrado":
	    statusEnum = StatusEnum.CLOSED;
	    break;
	case "Derivado":
	case "Infundado Collect":
	    statusEnum = StatusEnum.IN_PROGRESS;
	    break;
	case "Informacion Pendiente":
	    statusEnum = StatusEnum.PENDING;
	    break;
	case "SAR Procedente":
	case "Solucionado / encuesta":
	case "Tratamiento Comercial SC":
	    statusEnum = StatusEnum.RESOLVED;
	    break;
	default:
	    statusEnum = StatusEnum.NEW;
	}

	return statusEnum.toString();

    }

    public static String fromCMS(String status) {

	StatusEnum statusEnum;

	switch (status) {
	case "Masiva":
	    statusEnum = StatusEnum.IN_PROGRESS;
	    break;
	case "No Procede":
	    statusEnum = StatusEnum.CANCELLED;
	    break;
	case "Pendiente":
	    statusEnum = StatusEnum.PENDING;
	    break;
	case "Completado":
	    statusEnum = StatusEnum.RESOLVED;
	    break;
	case "Rechazado":
	    statusEnum = StatusEnum.REJECTED;
	    break;
	case "Asignado":
	    statusEnum = StatusEnum.ASSIGNED;
	    break;
	default:
	    statusEnum = StatusEnum.NEW;
	}

	return statusEnum.toString();
    }

    public static String fromGestel(String status) {

	StatusEnum statusEnum;

	switch (status) {
	case "REGISTRADO":
	    statusEnum = StatusEnum.NEW;
	    break;
	case "PENDIENTE":
	    statusEnum = StatusEnum.PENDING;
	    break;
	case "LIQUIDADO":
	case "CERRADO":
	    statusEnum = StatusEnum.CLOSED;
	    break;
	case "ANULADO":
	    statusEnum = StatusEnum.REJECTED;
	    break;
	case "CANCELADO":
	    statusEnum = StatusEnum.CANCELLED;
	    break;
	default:
	    statusEnum = StatusEnum.NEW;
	}

	return statusEnum.toString();
    }

}
