package com.telefonica.failure.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.failure.entity.Gestel;

/**
 * 
 * @Author:
 * @Datecreation: apr. 2020
 * @FileName: GestelRepository.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Repository de la clase Gestel.
 */
@Repository
public interface GestelRepository extends JpaRepository<Gestel, String> {
	
	/**
	 * Metodo Jpa que representa el Query: Select * from AVERIAS_GESTEL where NUM_TEL = id
	 * @param id
	 * @return List<Gestel>
	 */
	public abstract  List<Gestel> findByTelephoneNumber(String id);
	
	/**
	 * Metodo Jpa que representa el Query: Select * from AVERIAS_GESTEL where NUM_TEL = id and FECHA_HORA_AVERIA > startDate
	 * @param id
	 * @return List<Gestel>
	 */
	public abstract  List<Gestel> findByTelephoneNumberAndFailureDateAfter(String id, Date startDate );

}
