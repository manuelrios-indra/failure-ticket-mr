package com.telefonica.failure.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.failure.entity.ALDM;

/**
 * 
 * @Author:
 * @Datecreation: apr. 2020
 * @FileName: ALDMRepository.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Repository de la clase ALDM.
 */
@Repository
public interface ALDMRepository extends JpaRepository<ALDM, Long> {
	
	/**
	 * Metodo Jpa que representa el Query: Select * from AVERIAS_ALDM where NUM_TEL = id
	 * @param id
	 * @return List<ALDM>
	 */
	public abstract  List<ALDM> findByTelephoneNumber(String id);
	
	/**
	 * Metodo Jpa que representa el Query: Select * from AVERIAS_ALDM where NUM_TEL = id and FECHA_HORA_AVERIA > startDate
	 * @param id
	 * @return List<ALDM>
	 */
	public abstract  List<ALDM> findByTelephoneNumberAndFailureDateAfter(String id, Date startDate );

}
