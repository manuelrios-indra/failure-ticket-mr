package com.telefonica.failure.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.failure.entity.CMS;

/**
 * 
 * @Author:
 * @Datecreation: apr. 2020
 * @FileName: CMSRepository.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Repository de la clase CMS.
 */
@Repository
public interface CMSRepository extends JpaRepository<CMS, String> {
	
	/**
	 * Metodo Jpa que representa el Query: Select * from AVERIAS_CMS where COD_SERV_TV = id
	 * @param id
	 * @return List<CMS>
	 */
	public abstract  List<CMS> findByTvServiceCode(String id);
	
	/**
	 * Metodo Jpa que representa el Query: Select * from AVERIAS_CMS where COD_SERV_TV = id and FECHA_HORA_AVERIA > startDate
	 * @param id
	 * @return List<CMS>
	 */
	public abstract  List<CMS> findByTvServiceCodeAndFailureDateAfter(String id, Date startDate );

}
